#!/bin/sh
## Clean-up script for temporary files

baseDir=`dirname $0`
source $baseDir/config.sh 

# Thar scan data
cd ${CL_TharPath}/run/data/
for pippo in `find . -type d -mtime +${TharCleanDataInterval}`; do rm -r $pippo; done

# Thar log files
cd ${CL_RollingScanLogs}
for pippo in `find . -type d -mtime +${TharCleanDataInterval}`; do rm $pippo; done

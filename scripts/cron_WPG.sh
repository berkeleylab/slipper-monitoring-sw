#!/bin/bash
## Cron-job for Web-Page-Generator

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

cd ${DataPath}/temp/WPG/

# Generate list of variables for substitution
requestLockFile "lockWPG"
if [ $? -ne 0 ]; then
    #timed out or other general error
    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 2 \
	   --message "${progName} failed requesting lock." --db
    LogTS "${progName} failed requesting lock."
    exit 1
fi

LogTS "${progName}: Fetching data from DB"
python ${PythonScripts}/WebPage_FetchData.py ${CONFIG_WPG}

# Apply substitution to template input files
LogTS "${progName}: Applying substitution to template files"
cp ${BasePath}/slipper-monitoring-sw/Raw_HTML/index.html.zsc ./index.html
cp ${BasePath}/slipper-monitoring-sw/Raw_HTML/data.js.zsc ./data.js
while IFS='' read -r line || [[ -n "$line" ]]; do
    var=`echo $line | cut -d'=' -f1`
    value=`echo $line | cut -d'=' -f2-`
    
    ## index.html
    sed -i -e "s|#__${var}__#|${value}|g"  ./index.html
    if [ $? -ne 0 ]; then
	python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 3 \
	   --message "${progName} failed in variable substitution" --db	
	LogTS "${progName} Invalid substitution:"
	LogTS "VAR = ${var}"
	LogTS "VALUE = ${value}"
	LogTS "File = index.html"
	LogTS "s|#__${var}__#|${value}|g"
    fi

    ## data.js
    sed -i -e "s|#__${var}__#|${value}|g" ./data.js
    if [ $? -ne 0 ]; then
	python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 3 \
	   --message "${progName} failed in variable substitution" --db
	LogTS "${progName} Invalid substitution:"
	LogTS "VAR = ${var}"
	LogTS "VALUE = ${value}"
	LogTS "File = data.js"	
	LogTS "s|#__${var}__#|${value}|g"
    fi
    
done < WPGVars.txt


# Copy output files to web server folder
cp index.html ${WebHome}/
cp data.js ${WebHome}/

LogTS "${progName}: All done."
releaseLockFile "lockWPG"

#!/bin/bash
## Cron-job for Arduino_IO.py

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

requestArduinoLock
#save results qwith a consistent timestamp
if ! [ -z "$1" ]; then
    Ard_TS=$1
else
    Ard_TS=`date +%s`
fi
LogTS "Arduino TS"

python ${PythonScripts}/Arduino_IO.py ${CONFIG_AIO} --timestamp ${Ard_TS}
releaseArduinoLock

#read currents
cd ${CL_TharPath}/run
requestUSBLock
SCC_A_Ia=`../bin/rigol_ctrl --port ${CL_PS_1} --channel 1 get-current`
if [[ $? -ne 0 ]] || [[ -z "${SCC_A_Ia}" ]]; then SCC_A_Ia=-1; fi
SCC_A_Id=`../bin/rigol_ctrl --port ${CL_PS_1} --channel 2 get-current`
if [[ $? -ne 0 ]] || [[ -z "${SCC_A_Id}" ]]; then SCC_A_Id=-1; fi
SCC_B_Ia=0
SCC_B_Id=0
SCC_C_Ia=0
SCC_C_Id=0
updateArduinoQuery="UPDATE ARDUINO_IO SET"
updateArduinoQuery="${updateArduinoQuery} SCC_A_Digital_I = ${SCC_A_Id}, SCC_A_Analog_I = ${SCC_A_Ia},"
updateArduinoQuery="${updateArduinoQuery} SCC_B_Digital_I = ${SCC_B_Id}, SCC_B_Analog_I = ${SCC_B_Ia},"
updateArduinoQuery="${updateArduinoQuery} SCC_C_Digital_I = ${SCC_C_Id}, SCC_C_Analog_I = ${SCC_C_Ia} "
updateArduinoQuery="${updateArduinoQuery} WHERE TS=${Ard_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${updateArduinoQuery};"
releaseUSBLock

#re-generate web page
${BashScripts}/cron_WPG.sh

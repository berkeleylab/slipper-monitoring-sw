#!/bin/bash

baseDir=`dirname $0`
source $baseDir/config.sh

## Code
cd ${SSL_TharBuild}/run
echo "Running digital scans from ${PWD}."
echo "Saving data every ${SSL_saveInterval} into ${SSL_outputData}"
echo "Interrupt with CTRL+C"

LastSavedTS=0

while true; do    
    
    timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogscan.json -p -m 0 >& ${SSL_outputLog}/last.log
    
    #save results qwith a consistent timestamp
    TS=`date +%s`
    
    #save temperature
    python ${PythonScripts}/Arduino_IO.py ${CONFIG_AIO} --timestamp ${TS} >& /dev/null
    
    #read currents
    SCC_A_Ia=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 get-current`
    if [[ $? -ne 0 ]] || [[ -z "${SCC_A_Ia}" ]]; then SCC_A_Ia=-1; fi
    SCC_A_Id=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 get-current`
    if [[ $? -ne 0 ]] || [[ -z "${SCC_A_Id}" ]]; then SCC_A_Id=-1; fi
    SCC_B_Ia=0
    SCC_B_Id=0
    SCC_C_Ia=0
    SCC_C_Id=0
    updateArduinoQuery="UPDATE ARDUINO_IO SET"
    updateArduinoQuery="${updateArduinoQuery} SCC_A_Digital_I=${SCC_A_Id},SCC_A_Analog_I=${SCC_A_Ia},"
    updateArduinoQuery="${updateArduinoQuery} SCC_B_Digital_I=${SCC_B_Id},SCC_B_Analog_I=${SCC_B_Ia},"
    updateArduinoQuery="${updateArduinoQuery} SCC_C_Digital_I=${SCC_C_Id},SCC_C_Analog_I=${SCC_C_Ia} "
    updateArduinoQuery="${updateArduinoQuery} WHERE TS=${TS}"
    mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${updateArduinoQuery};"
    
    #temporary save each log file
    cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log

    #check PS status and take actions if necessary
    V_1=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 get-voltage`
    V_1_on=`echo "${V_1} > 0.1" | bc`
    V_2=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 get-voltage`
    V_2_on=`echo "${V_2} > 0.1" | bc`
    if [[ "${V_1_on}" -eq 0 ]] && [[ "${V_2_on}" -eq 0 ]]; then
	#PS went off. check last time it happened
	lastPSActionTS=`tail -1 .lastPSAction`
	[[ -z "${lastPSActionTS}" ]] && lastPSActionTS=0	
	lastPSActionInterval=$(( TS - lastPSActionTS ))

	#if more than 1hr ago, try to power it on again
	#and save now as last time this was powered on	
	if [[ ${lastPSActionInterval} -ge 1800 ]]; then
	    ../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 power-on 1.85 2.0
	    ../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 power-on 2.0 2.0
	    echo "${TS}" >> .lastPSAction
	    updateLogQuery="INSERT INTO Log (TS, Source_ID, Type_ID, Message) VALUES (${TS}, 3, 1, 'Powered ON PS');"
	    mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${updateLogQuery};"
	else
	    echo "${TS}" >> .PSOffNoAction
	fi	    	
    fi
    
    V_3=`../bin/rigol_ctrl --port ${SSL_PS_2} --channel 3 get-voltage`
    V_3_on=`echo "${V_3} > 0.1" | bc`
    if [[ "${V_3_on}" -eq 0 ]]; then
	#PS went off. check last time it happened
	lastPS3ActionTS=`tail -1 .lastPS3Action`
	[[ -z "${lastPS3ActionTS}" ]] && lastPS3ActionTS=0	
	lastPS3ActionInterval=$(( TS - lastPS3ActionTS ))
	
	#if more than 1hr ago, try to power it on again
	#and save now as last time this was powered on	
	if [[ ${lastPS3ActionInterval} -ge 3600 ]]; then
	    ../bin/rigol_ctrl --port ${SSL_PS_2} --channel 3 power-on 3.3 0.2
	    echo "${TS}" >> .lastPS3Action
	    updateLogQuery="INSERT INTO Log (TS, Source_ID, Type_ID, Message) VALUES (${TS}, 3, 1, 'Powered ON ADC PS');"
	    mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${updateLogQuery};"
	else
	    echo "${TS}" >> .PS3OffNoAction
	fi
    fi
	
	
    #save scan permanently every once in a while
    if [[ $((TS - LastSavedTS)) -ge ${SSL_saveInterval} ]]; then
	echo "Saving scan results"
	
	#save scan results to output location and to DB
	folderToSave=`ls -1tr data/ | tail -1`            
	cp -r data/${folderToSave} ${SSL_outputData}
	insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
	insertChipData="${insertChipData} (${TS}, 1, 0, '${SSL_outputData}/${folderToSave}')"
	mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
	
	#update monitoring page result (this could be done more often actually, or moved to the WebPageGenerator)
	cp data/${folderToSave}/0x0778_OccupancyMap.png ${WebHome}/OccupancyMap/OccupancyMap_SCC_A.png
	python ${BashScripts}/cron_WPG.sh
	
	LastSavedTS=${TS}      
    fi

    #Sleep a bit before starting over, if needed
    sleep 1    
    
done

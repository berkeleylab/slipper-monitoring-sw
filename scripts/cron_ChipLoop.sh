#!/bin/bash
# Check if ChipLoop is running, if not submit new job

## Init

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

#check if it is running
if [ -w /local/slipper/slipper-data/perm/log/ChipLoop_PID.log ]
then
   PID=`cat /local/slipper/slipper-data/perm/log/ChipLoop_PID.log`
   if ps -p $PID > /dev/null
   then
      LogTS "ChipLoop check, PID ${PID}"
      exit
   fi
fi

#submit new job
CurrentDate=`date +%Y-%m-%d`
nohup sh /local/slipper/slipper-monitoring-sw/scripts/ChipLoop.sh >& /local/slipper/slipper-data/perm/log/${CurrentDate}_CL.log &
New_PID=$!
echo ${New_PID} > /local/slipper/slipper-data/perm/log/ChipLoop_PID.log
python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 5 \
--message "${progName} is submitting new ChipLoop job with PID ${New_PID}." --db
if [ -z "${New_PID}" ]
then
	python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 6 \
	--message "${progName} failed to submit new ChipLoop job." --db
fi


#!/bin/bash
SlipperVersion="V7.3"

#-----------------------------------------------------
#------------------Global Arguments-------------------
#-----------------------------------------------------
BasePath=/local/slipper
#BasePath=/home/adimitri
#BasePath=/home/spagan/working/pixels/slipper/
PythonScripts=${BasePath}/slipper-monitoring-sw/python/
BashScripts=${BasePath}/slipper-monitoring-sw/scripts/
DataPath=${BasePath}/slipper-data
WebHome=/home/slipper/public_html/
#WebHome=${BasePath}/slipper-monitoring-sw/WebPage/

#-----------------------------------------------------
#------------------Database Arguments-------------------
#-----------------------------------------------------
host='localhost' ## The host of the current database
Database='slipper' ## Current database
LogTable='Log' ## Name of the Log table
LogUser='slipper' ## UserName Specailly for Log_ADD
LogPassword='pxKr_LOG' ## Password Specailly for Log_ADD
LogFilesPath="${DataPath}/perm/log" ## General log path
#Bind the above together
CommonCONFIG="--host ${host} --database ${Database} --LogTable ${LogTable} --user ${LogUser} --password ${LogPassword}"

#-----------------------------------------------------
#------------------ARDUINO_IO Arguments---------------
#-----------------------------------------------------
AIO_LogFile="${LogFilesPath}/ArduinoIO.log" ## Name of the Log File
AIO_port='/dev/ttyArd0'
AIO_baudrate='9600'
AIO_timeout='1'
AIO_Table='ARDUINO_IO'
AIO_InputExpect='$T@Env_Temp$H@Env_Humidity$NTC@SCC_A_NTC$VDDA@SCC_A_Vdda$VDDD@SCC_A_Vddd$VrefA@SCC_A_VRefa$VrefD@SCC_A_VRefd$'
#AIO_Debug='--Debug' ## Uncomment to enable debug info
#Bind the above together
CONFIG_AIO="${CommonCONFIG} --LogFile ${AIO_LogFile} --port ${AIO_port} --baudrate ${AIO_baudrate} --timeout ${AIO_timeout} --table ${AIO_Table} --InputExpect ${AIO_InputExpect}"
#echo $CONFIG_AIO

#-----------------------------------------------------
#-------------Web_Page_Generator Arguments------------
#-----------------------------------------------------
WPG_LogFile="${LogFilesPath}/WPG.log"
#WPG_Debug="--Debug" # uncomment to enable debug into log file
#Bind the above together
CONFIG_WPG="${CommonCONFIG} --LogFile ${WPG_LogFile} ${WPG_Debug}"

#-----------------------------------------------------
#------------------Main data taking-------------------
#-----------------------------------------------------
CL_TharPath=${BasePath}/Thar
CL_RollingScanLogs="${DataPath}/temp/scan-log/"
CL_ScansPath=${DataPath}/perm/scan
CL_InitialScansPath=${DataPath}/perm/custom-scans/scan-fixed_tuning
CL_TuningPath=${DataPath}/perm/tuning
CL_ChipName_SCC_A="0x0798"
CL_ChipTable="Chip_Data"
CL_PS_1='/dev/usbtmc0' #Chip PS
CL_PS_2='/dev/usbtmc1' #ADC PS
CL_SourcePosition_SCC_A=2  #0=absent, 1=left, 2=right

#-----------------------------------------------------
#------------------Simple Scan Loop--------------------
#-----------------------------------------------------
SSL_TharBuild=${BasePath}/Thar/
SSL_saveInterval=3600
SSL_saveTuneInterval=86400
SSL_outputData=${BasePath}/saved-data
SSL_outputDataMUX=${BasePath}/saved-dataMUX
SSL_outputLog=${BasePath}/saved-log
SSL_ChipTable="Chip_Data"
SSL_ChipName_SCC_A="0x0798"
SSL_PS_1='/dev/usbtmc0'
SSL_PS_2='/dev/usbtmc1'
SSL_SourcePosition_SCC_A=2 #0=absent, 1=left, 2=right

#-----------------------------------------------------
#------------------Clean-up cron--------------------
#-----------------------------------------------------
TharCleanDataInterval=5 #days

#-----------------------------------------------------
#------------------Other Arguments--------------------
#-----------------------------------------------------
BACKUP_FolderDB=${DataPath}/perm/db

#-----------------------------------------------------
#------------------Utility functions--------------------
#-----------------------------------------------------
UTIL_LockFolder=${DataPath}

## Request exclusive access to PCIe card for YARR
## Arguments:
## $1: lock name
## $2: timeout 
## Returns:
## 0: lock successfully obtained
## 1: no lock obtained within timeout
function requestLockFile {
    lockFile=$1
    if [ -z "${lockFile}" ]; then
	lockFile=".lockSlipper" #default name
    fi
    lockFile="${UTIL_LockFolder}/.${lockFile}"
    timeout=$2
    if [ -z "${timeout}" ]; then
	timeout=60 #s
    fi
    Lock_TS=`date +%s`
    lockObtained=0
    while true; do
	if ! [ -f ${lockFile} ]; then
	    #Successfully got lock
	    touch ${lockFile}
	    lockObtained=1
	    break
	fi
	newTS=`date +%s`
	timeElapsed=$((newTS - Lock_TS))
	if [ $timeElapsed -ge $timeout ]; then
	    #timeout reached, return
	    return 1
	fi
	sleep 1
    done

    trap "python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 7 \
    	         --message \"${progName} exited unexpectedly. Cleaning up lock file: $1\" --db; \
		 releaseLockFile $1" EXIT
    
    return 0
}

## Release lock file
function releaseLockFile {
    lockFile=$1
    if [ -z "${lockFile}" ]; then
	lockFile=".lockSlipper" #default name
    fi
    rm -f "${UTIL_LockFolder}/.${lockFile}"
    #release trap
    trap '' EXIT    
}

## Define lock functions for specific usages
## Arguments:
## $1: timeout (see above)
## Returns: see general requestLockFile above

function requestYarrLock {
    timeout=$1
    if [ -z "${timeout}" ]; then
	timeout=1800 #s
    fi
    requestLockFile "YarrLockFile" $timeout
}

function releaseYarrLock {
    releaseLockFile "YarrLockFile"
}

function requestArduinoLock {
    timeout=$1
    if [ -z "${timeout}" ]; then
	timeout=60 #s
    fi
    requestLockFile "ArduinoLockFile" $timeout
}

function releaseArduinoLock {
    releaseLockFile "ArduinoLockFile"
}

function requestUSBLock {
    timeout=$1
    if [ -z "${timeout}" ]; then
	timeout=60 #s
    fi
    requestLockFile "USBLockFile" $timeout
}

function releaseUSBLock {
    releaseLockFile "USBLockFile"
}

# Function to print to screen/log-file with timestamp
# Arguments:
# $1: Message
function LogTS {
    echo `date +"%D %T"` $1
}

# Function to set a timeout and limit memory usage of processes
# Arguments:
# $1: timeout (formatted as argument of timeout bash command)
# $2: meomry limit in MB
function wrapExec {
    time_limit=$1
    memory_limit=$2
    shift;shift;
    ulimit -S -v $((${memory_limit} * 1024))
    timeout ${time_limit} $* 
    ulimit -v unlimited
}

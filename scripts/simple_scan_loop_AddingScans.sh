#!/bin/bash

baseDir=`dirname $0`
source $baseDir/config.sh

## Code
cd ${SSL_TharBuild}/run
echo "Running digital scans from ${PWD}."
echo "Saving data every ${SSL_saveInterval} into ${SSL_outputData}"
echo "Interrupt with CTRL+C"

LastSavedTS=0
LastSavedTuneTS=0

while true; do    
    
    #timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogscan.json -p -m 0 >& ${SSL_outputLog}/last.log
    #timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogGPscan.json -p -m 0 >& ${SSL_outputLog}/last.log 
    #noise scan with global pulse for ring oscilators (either we run noiseGPscan or analogGPscan which also has the global pulse for the ring oscilators
    timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_noiseGPscan.json -p -m 0 >& ${SSL_outputLog}/last.log
    #save results qwith a consistent timestamp
    TS=`date +%s`
    
    #save temperature
    python ${PythonScripts}/Arduino_IO.py ${CONFIG_AIO} --timestamp ${TS} >& /dev/null
    
    #read currents
    SCC_A_Ia=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 get-current`
    if [[ $? -ne 0 ]] || [[ -z "${SCC_A_Ia}" ]]; then SCC_A_Ia=-1; fi
    SCC_A_Id=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 get-current`
    if [[ $? -ne 0 ]] || [[ -z "${SCC_A_Id}" ]]; then SCC_A_Id=-1; fi
    SCC_B_Ia=0
    SCC_B_Id=0
    SCC_C_Ia=0
    SCC_C_Id=0
    updateArduinoQuery="UPDATE ARDUINO_IO SET"
    updateArduinoQuery="${updateArduinoQuery} SCC_A_Digital_I=${SCC_A_Id},SCC_A_Analog_I=${SCC_A_Ia},"
    updateArduinoQuery="${updateArduinoQuery} SCC_B_Digital_I=${SCC_B_Id},SCC_B_Analog_I=${SCC_B_Ia},"
    updateArduinoQuery="${updateArduinoQuery} SCC_C_Digital_I=${SCC_C_Id},SCC_C_Analog_I=${SCC_C_Ia} "
    updateArduinoQuery="${updateArduinoQuery} WHERE TS=${TS}"
    mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${updateArduinoQuery};"
    
    #temporary save each log file
    cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log

    #update web page
    python ${BashScripts}/cron_WPG.sh

    #check PS status and take actions if necessary
    V_1=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 get-voltage`
    V_1_on=`echo "${V_1} > 0.1" | bc`
    V_2=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 get-voltage`
    V_2_on=`echo "${V_2} > 0.1" | bc`
    if [[ "${V_1_on}" -eq 0 ]] && [[ "${V_2_on}" -eq 0 ]]; then
	#PS went off. check last time it happened
	lastPSActionTS=`tail -1 .lastPSAction`
	[[ -z "${lastPSActionTS}" ]] && lastPSActionTS=0	
	lastPSActionInterval=$(( TS - lastPSActionTS ))

	#if more than 1hr ago, try to power it on again
	#and save now as last time this was powered on	
	if [[ ${lastPSActionInterval} -ge 1800 ]]; then
	    ../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 power-on 1.85 2.0
	    sleep 1
	    ../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 power-on 1.85 2.0
	    echo "${TS}" >> .lastPSAction
	    updateLogQuery="INSERT INTO Log (TS, Source_ID, Type_ID, Message) VALUES (${TS}, 3, 1, 'Powered ON PS');"
	    mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${updateLogQuery};"
	else
	    echo "${TS}" >> .PSOffNoAction
	fi	    	
    fi
    
    V_3=`../bin/rigol_ctrl --port ${SSL_PS_2} --channel 3 get-voltage`
    V_3_on=`echo "${V_3} > 0.1" | bc`
    if [[ "${V_3_on}" -eq 0 ]]; then
	#PS went off. check last time it happened
	lastPS3ActionTS=`tail -1 .lastPS3Action`
	[[ -z "${lastPS3ActionTS}" ]] && lastPS3ActionTS=0	
	lastPS3ActionInterval=$(( TS - lastPS3ActionTS ))
	
	#if more than 1hr ago, try to power it on again
	#and save now as last time this was powered on	
	if [[ ${lastPS3ActionInterval} -ge 3600 ]]; then
	    ../bin/rigol_ctrl --port ${SSL_PS_2} --channel 3 power-on 3.3 0.2
	    echo "${TS}" >> .lastPS3Action
	    updateLogQuery="INSERT INTO Log (TS, Source_ID, Type_ID, Message) VALUES (${TS}, 3, 1, 'Powered ON ADC PS');"
	    mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${updateLogQuery};"
	else
	    echo "${TS}" >> .PS3OffNoAction
	fi
    fi
	
	
    #save scan permanently every once in a while
    if [[ $((TS - LastSavedTS)) -ge ${SSL_saveInterval} ]]; then
	echo "Saving scan results"

        #run digital scan and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p -m 0 >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

	#run analog scan and save results
	timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogscan.json -p -m 0 >& ${SSL_outputLog}/last.log
	cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
	folderToSave=`ls -1tr data/ | tail -1`            
	cp -r data/${folderToSave} ${SSL_outputData}
	insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
	insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
	mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"	
	cp data/${folderToSave}/${SSL_ChipName_SCC_A}_OccupancyMap.png ${WebHome}/OccupancyMap/OccupancyMap_SCC_A.png

	#run threshold scan and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_thresholdscan.json -p -m 0 >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        #run tot scan and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_totscan.json -p -t 3000 -m 0 >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

	#run MUX scan and save results
        timeout 10m ../bin/rd53a_MonitorMUX /dev/ttyUSB0 5 ${SSL_ChipName_SCC_A} configs/rd53a_test.json
        folderToSave=`ls -1tr . | tail -1`
	mkdir ${SSL_outputDataMUX}/${TS}
        mv ${folderToSave}/*.dat ${SSL_outputDataMUX}/${TS}
	IRef_Value=`awk '/0 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_CurrentMonitorVmV_MonitorImonMux.dat | sed -n 1p`
	Temperature_Sensor_1_Value=`awk '/3 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        Temperature_Sensor_2_Value=`awk '/5 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        Temperature_Sensor_3_Value=`awk '/15 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        Temperature_Sensor_4_Value=`awk '/7 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        Radiation_Sensor_1_Value=`awk '/4 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        Radiation_Sensor_2_Value=`awk '/6 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        Radiation_Sensor_3_Value=`awk '/14 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        Radiation_Sensor_4_Value=`awk '/8 /{print $NF}' ${SSL_outputDataMUX}/${TS}/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path, Temperature_Sensor_1, Temperature_Sensor_2, Temperature_Sensor_3, Temperature_Sensor_4, Radiation_Sensor_1, Radiation_Sensor_2, Radiation_Sensor_3, Radiation_Sensor_4, IRef) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputDataMUX}/${TS}', ${Temperature_Sensor_1_Value}, ${Temperature_Sensor_2_Value}, ${Temperature_Sensor_3_Value}, ${Temperature_Sensor_4_Value}, ${Radiation_Sensor_1_Value}, ${Radiation_Sensor_2_Value}, ${Radiation_Sensor_3_Value}, ${Radiation_Sensor_4_Value}, ${IRef_Value})"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        #run Ring Oscilators and save results
        timeout 10m ../bin/rd53a_test_ringosc -i 20 -d 6 -r 10 -o ${SSL_outputData}/ring_osc.log
	Ring_Oscillators_1_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 5p`
        Ring_Oscillators_2_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 9p`
        Ring_Oscillators_3_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 13p`
        Ring_Oscillators_4_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 17p`
        Ring_Oscillators_5_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 7p`
        Ring_Oscillators_6_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 11p`
        Ring_Oscillators_7_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 15p`
        Ring_Oscillators_8_Value=`less ${SSL_outputData}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 19p`
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path, Ring_Oscillators_1, Ring_Oscillators_2, Ring_Oscillators_3, Ring_Oscillators_4, Ring_Oscillators_5, Ring_Oscillators_6, Ring_Oscillators_7, Ring_Oscillators_8) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/ring_osc.log', ${Ring_Oscillators_1_Value}, ${Ring_Oscillators_2_Value}, ${Ring_Oscillators_3_Value}, ${Ring_Oscillators_4_Value}, ${Ring_Oscillators_5_Value}, ${Ring_Oscillators_6_Value}, ${Ring_Oscillators_7_Value}, ${Ring_Oscillators_8_Value})"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        #run noise scan and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_noisescan.json -p -m 0 >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
	cp data/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMask.png ${WebHome}/MaskMap/NoiseMask_SCC_A.png

	#update Web page
	python ${BashScripts}/cron_WPG.sh	

	LastSavedTS=${TS}      
    fi

    #run tuning and save output
    if [[ $((TS - LastSavedTuneTS)) -ge ${SSL_saveTuneInterval} ]]; then
        echo "Saving tuning results"

        #reset the configuration file before tuning
	rm configs/rd53a_test.json
	cp configs/rd53a_default.json configs/rd53a_test.json

        #run digital scan and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        #run analog scan and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogscan.json -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"


	#run tuning scan and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_tune_globalthreshold.json -t 1500 -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_tune_globalthreshold.json -t 1000 -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log

        #save scan results to output location and to DB
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/syn_tune_globalthreshold.json -t 1250 -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_tune_pixelthreshold.json -t 1250 -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_tune_pixelthreshold.json -t 1500 -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_tune_pixelthreshold.json -t 1000 -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_thresholdscan.json -m 0 -p >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

        #run noise scan and mask nosiy pixels and save results
        timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_noisescan.json -p -m 0 >& ${SSL_outputLog}/last.log
        cp ${SSL_outputLog}/last.log ${SSL_outputLog}/${TS}.log
        folderToSave=`ls -1tr data/ | tail -1`
        cp -r data/${folderToSave} ${SSL_outputData}
        insertChipData="INSERT INTO ${SSL_ChipTable} (TS, Chip_ID, Source_Position, Data_Path) VALUES"
        insertChipData="${insertChipData} (${TS}, 1, ${SSL_SourcePosition_SCC_A}, '${SSL_outputData}/${folderToSave}')"
        mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
	cp data/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMask.png ${WebHome}/MaskMap/NoiseMask_SCC_A.png

	#update web page
	python ${BashScripts}/cron_WPG.sh	

        LastSavedTuneTS=${TS}
    fi

    #Sleep a bit before starting over, if needed
    sleep 1    
    
done

#!/bin/bash
# Perform a full re-tuning of the chip

## Init

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

# request YARR lock, default timeout
requestYarrLock
if [ $? -ne 0 ]; then
    #timed out or other general error
    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 2 \
	   --message "${progName} failed requesting Yarr lock." --db
    exit 1
fi

## Run
cd ${CL_TharPath}/run

#save results qwith a consistent timestamp
Tuning_TS=`date +%s`
LogTS "Tuning TS"
mkdir ${CL_TuningPath}/${Tuning_TS}

#reset the configuration file before tuning
cp configs/rd53a_default.json configs/rd53a_test.json

#run digital scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -m 1 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/std_digitalscan.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/
insertChipData="INSERT INTO ${CL_ChipTable} (TS, Chip_ID, Source_Position, Scan_Type, Data_Path) VALUES"
insertChipData="${insertChipData} (${Tuning_TS}, $(( ${CL_ChipName_SCC_A} )), ${CL_SourcePosition_SCC_A}, 1, '${CL_TuningPath}/${Tuning_TS}')"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Occupancy ${CL_TuningPath}/${Tuning_TS}/${folderToSave} >& rootoutput.log
DigiScan_Average_Occupancy_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
DigiScan_Average_Occupancy_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
DigiScan_Average_Occupancy_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET DigiScan_Average_Occupancy_FE_1 = ${DigiScan_Average_Occupancy_FE_1_Value}, DigiScan_Average_Occupancy_FE_2 = ${DigiScan_Average_Occupancy_FE_2_Value}, DigiScan_Average_Occupancy_FE_3 = ${DigiScan_Average_Occupancy_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${Tuning_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

#run analog scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogscan.json -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/std_analogscan.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Occupancy ${CL_TuningPath}/${Tuning_TS}/${folderToSave} >& rootoutput.log
AnalogScan_Average_Occupancy_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
AnalogScan_Average_Occupancy_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
AnalogScan_Average_Occupancy_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET AnalogScan_Average_Occupancy_FE_1 = ${AnalogScan_Average_Occupancy_FE_1_Value}, AnalogScan_Average_Occupancy_FE_2 = ${AnalogScan_Average_Occupancy_FE_2_Value}, AnalogScan_Average_Occupancy_FE_3 = ${AnalogScan_Average_Occupancy_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${Tuning_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
cp data/${folderToSave}/${CL_ChipName_SCC_A}_OccupancyMap.png ${WebHome}/Figures/OccupancyMap_SCC_A.png

#run tuning scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_tune_globalthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/diff_tune_globalthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_tune_pixelthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/diff_tune_pixelthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_tune_globalpreamp.json -t 10000 8 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/diff_tune_globalpreamp.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_tune_pixelthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/diff_retune_pixelthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_tune_globalthreshold.json -t 2000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/lin_tune_globalthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_tune_pixelthreshold.json -t 2000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/lin_tune_pixelthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_retune_globalthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/lin_retune_globalthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_retune_pixelthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/lin_retune_pixelthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_tune_globalpreamp.json -t 10000 8 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/lin_tune_globalpreamp.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/lin_retune_pixelthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/lin_reretune_pixelthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/syn_tune_globalthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/syn_tune_globalthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/syn_tune_globalpreamp.json -t 10000 8 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/syn_tune_globalpreamp.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/syn_tune_globalthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/syn_retune_globalthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_retune_globalthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/diff_reretune_globalthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/diff_retune_pixelthreshold.json -t 1000 -m 0 -p >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/diff_reretune_pixelthreshold.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/

#check if the value of the Lin FE threshold and use default pretuned config file if the tuning failed
LinTH_Value=`grep LinVth configs/rd53a_test.json | awk '{print $2}' | cut -d',' -f 1`
if [ ${LinTH_Value} -eq 0 ]; then 
    cp configs/rd53a_tunedAllCold.json configs/rd53a_test.json
    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 4 \
	--message "${progName} failed tuning of the LinFE." --db
fi

#run threshold scan and save results
timeout 15m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_thresholdscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/std_thresholdscan.log
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Threshold ${CL_TuningPath}/${Tuning_TS}/${folderToSave} >& rootoutput.log
ThrScan_Mean_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
ThrScan_Sigma_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 1p`
ThrScan_Mean_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
ThrScan_Sigma_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 2p`
ThrScan_Mean_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
ThrScan_Sigma_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET ThrScan_Mean_FE_1 = ${ThrScan_Mean_FE_1_Value}, ThrScan_Mean_FE_2 = ${ThrScan_Mean_FE_2_Value}, ThrScan_Mean_FE_3 = ${ThrScan_Mean_FE_3_Value}, ThrScan_Sigma_FE_1 = ${ThrScan_Sigma_FE_1_Value}, ThrScan_Sigma_FE_2 = ${ThrScan_Sigma_FE_2_Value}, ThrScan_Sigma_FE_3 = ${ThrScan_Sigma_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${Tuning_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Noise ${CL_TuningPath}/${Tuning_TS}/${folderToSave} >& rootoutput.log
ThrScan_Noise_Mean_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
ThrScan_Noise_Sigma_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 1p`
ThrScan_Noise_Mean_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
ThrScan_Noise_Sigma_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 2p`
ThrScan_Noise_Mean_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
ThrScan_Noise_Sigma_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET ThrScan_Noise_Mean_FE_1 = ${ThrScan_Noise_Mean_FE_1_Value}, ThrScan_Noise_Mean_FE_2 = ${ThrScan_Noise_Mean_FE_2_Value}, ThrScan_Noise_Mean_FE_3 = ${ThrScan_Noise_Mean_FE_3_Value}, ThrScan_Noise_Sigma_FE_1 = ${ThrScan_Noise_Sigma_FE_1_Value}, ThrScan_Noise_Sigma_FE_2 = ${ThrScan_Noise_Sigma_FE_2_Value}, ThrScan_Noise_Sigma_FE_3 = ${ThrScan_Noise_Sigma_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${Tuning_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
cp ${CL_TuningPath}/${Tuning_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_ThresholdMap_PLOT.png ${WebHome}/Figures/ThMap_SCC_A.png
cp ${CL_TuningPath}/${Tuning_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_ThresholdMap_STACK.png ${WebHome}/Figures/ThDist_SCC_A.png
cp ${CL_TuningPath}/${Tuning_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMap_PLOT.png ${WebHome}/Figures/NoiseMap_SCC_A.png
cp ${CL_TuningPath}/${Tuning_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMap_STACK.png ${WebHome}/Figures/NoiseDist_SCC_A.png

#run tot scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_totscan.json -p -t 10000 -m 0 >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/std_totscan.log
timeout 5m ../external/YARR/src/scripts/plotWithRoot_ToT ${CL_TuningPath}/${Tuning_TS}/${folderToSave} >& rootoutput.log
ToTScan_Mean_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
ToTScan_Mean_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
ToTScan_Mean_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET ToTScan_Mean_FE_1 = ${ToTScan_Mean_FE_1_Value}, ToTScan_Mean_FE_2 = ${ToTScan_Mean_FE_2_Value}, ToTScan_Mean_FE_3 = ${ToTScan_Mean_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${Tuning_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
cp ${CL_TuningPath}/${Tuning_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_MeanTotMap0_PLOT.png ${WebHome}/Figures/TotMap_SCC_A.png
cp ${CL_TuningPath}/${Tuning_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_MeanTotMap0_STACK.png ${WebHome}/Figures/TotDist_SCC_A.png

#run noise scan and (potentially) mask nosiy pixels and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_noisescan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_TuningPath}/${Tuning_TS}/std_noisescan.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_TuningPath}/${Tuning_TS}/
cp data/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMask.png ${WebHome}/Figures/NoiseMask_SCC_A.png


## Finalization
#release lock file
releaseYarrLock

#update Web page
${BashScripts}/cron_WPG.sh


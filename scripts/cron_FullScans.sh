#!/bin/bash
# Perform a full set of scans and save results

## Init

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

# request YARR lock, default timeout
requestYarrLock
if [ $? -ne 0 ]; then
    #timed out or other general error
    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 2 \
	   --message "${progName} failed requesting Yarr lock." --db
    exit 1
fi

## Run
cd ${CL_TharPath}/run

#save results qwith a consistent timestamp
FullScan_TS=`date +%s`
LogTS "FullScan TS"
mkdir ${CL_ScansPath}/${FullScan_TS}

#run digital scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_ScansPath}/${FullScan_TS}/
cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_digitalscan.log
#insert first results, will then update this record with other results
insertChipData="INSERT INTO ${CL_ChipTable} (TS, Chip_ID, Source_Position, Scan_Type, Data_Path) VALUES"
insertChipData="${insertChipData} (${FullScan_TS}, $(( ${CL_ChipName_SCC_A} )), ${CL_SourcePosition_SCC_A}, 2, '${CL_ScansPath}/${FullScan_TS}')"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Occupancy ${CL_ScansPath}/${FullScan_TS}/${folderToSave} >& rootoutput.log
DigiScan_Average_Occupancy_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
DigiScan_Average_Occupancy_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
DigiScan_Average_Occupancy_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET DigiScan_Average_Occupancy_FE_1 = ${DigiScan_Average_Occupancy_FE_1_Value}, DigiScan_Average_Occupancy_FE_2 = ${DigiScan_Average_Occupancy_FE_2_Value}, DigiScan_Average_Occupancy_FE_3 = ${DigiScan_Average_Occupancy_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

#run analog scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr data/ | tail -1`            
cp -r data/${folderToSave} ${CL_ScansPath}/${FullScan_TS}/
cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_analogscan.log
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Occupancy ${CL_ScansPath}/${FullScan_TS}/${folderToSave} >& rootoutput.log
AnalogScan_Average_Occupancy_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
AnalogScan_Average_Occupancy_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
AnalogScan_Average_Occupancy_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET AnalogScan_Average_Occupancy_FE_1 = ${AnalogScan_Average_Occupancy_FE_1_Value}, AnalogScan_Average_Occupancy_FE_2 = ${AnalogScan_Average_Occupancy_FE_2_Value}, AnalogScan_Average_Occupancy_FE_3 = ${AnalogScan_Average_Occupancy_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
cp data/${folderToSave}/${CL_ChipName_SCC_A}_OccupancyMap.png ${WebHome}/Figures/OccupancyMap_SCC_A.png

#run threshold scan and save results
timeout 15m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_thresholdscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_ScansPath}/${FullScan_TS}/
cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_thresholdscan.log
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Threshold ${CL_ScansPath}/${FullScan_TS}/${folderToSave} >& rootoutput.log
ThrScan_Mean_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
ThrScan_Sigma_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 1p`
ThrScan_Mean_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
ThrScan_Sigma_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 2p`
ThrScan_Mean_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
ThrScan_Sigma_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET ThrScan_Mean_FE_1 = ${ThrScan_Mean_FE_1_Value}, ThrScan_Mean_FE_2 = ${ThrScan_Mean_FE_2_Value}, ThrScan_Mean_FE_3 = ${ThrScan_Mean_FE_3_Value}, ThrScan_Sigma_FE_1 = ${ThrScan_Sigma_FE_1_Value}, ThrScan_Sigma_FE_2 = ${ThrScan_Sigma_FE_2_Value}, ThrScan_Sigma_FE_3 = ${ThrScan_Sigma_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
timeout 5m ../external/YARR/src/scripts/plotWithRoot_Noise ${CL_ScansPath}/${FullScan_TS}/${folderToSave} >& rootoutput.log
ThrScan_Noise_Mean_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
ThrScan_Noise_Sigma_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 1p`
ThrScan_Noise_Mean_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
ThrScan_Noise_Sigma_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 2p`
ThrScan_Noise_Mean_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
ThrScan_Noise_Sigma_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $3}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET ThrScan_Noise_Mean_FE_1 = ${ThrScan_Noise_Mean_FE_1_Value}, ThrScan_Noise_Mean_FE_2 = ${ThrScan_Noise_Mean_FE_2_Value}, ThrScan_Noise_Mean_FE_3 = ${ThrScan_Noise_Mean_FE_3_Value}, ThrScan_Noise_Sigma_FE_1 = ${ThrScan_Noise_Sigma_FE_1_Value}, ThrScan_Noise_Sigma_FE_2 = ${ThrScan_Noise_Sigma_FE_2_Value}, ThrScan_Noise_Sigma_FE_3 = ${ThrScan_Noise_Sigma_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
cp ${CL_ScansPath}/${FullScan_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_ThresholdMap_PLOT.png ${WebHome}/Figures/ThMap_SCC_A.png
cp ${CL_ScansPath}/${FullScan_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_ThresholdMap_STACK.png ${WebHome}/Figures/ThDist_SCC_A.png
cp ${CL_ScansPath}/${FullScan_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMap_PLOT.png ${WebHome}/Figures/NoiseMap_SCC_A.png
cp ${CL_ScansPath}/${FullScan_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMap_STACK.png ${WebHome}/Figures/NoiseDist_SCC_A.png

#run tot scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_totscan.json -p -t 10000 -m 0 >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_ScansPath}/${FullScan_TS}/
cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_totscan.log
timeout 5m ../external/YARR/src/scripts/plotWithRoot_ToT ${CL_ScansPath}/${FullScan_TS}/${folderToSave} >& rootoutput.log
ToTScan_Mean_FE_1_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 1p`
ToTScan_Mean_FE_2_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 2p`
ToTScan_Mean_FE_3_Value=`less rootoutput.log | grep FE | awk '{print $2}' | sed -n 3p`
insertChipData="UPDATE ${CL_ChipTable} SET ToTScan_Mean_FE_1 = ${ToTScan_Mean_FE_1_Value}, ToTScan_Mean_FE_2 = ${ToTScan_Mean_FE_2_Value}, ToTScan_Mean_FE_3 = ${ToTScan_Mean_FE_3_Value} "
insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
cp ${CL_ScansPath}/${FullScan_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_MeanTotMap0_PLOT.png ${WebHome}/Figures/TotMap_SCC_A.png
cp ${CL_ScansPath}/${FullScan_TS}/${folderToSave}/${SSL_ChipName_SCC_A}_MeanTotMap0_STACK.png ${WebHome}/Figures/TotDist_SCC_A.png

#run MUX scan and save results
mkdir ${CL_ScansPath}/${FullScan_TS}/MUX
timeout 10m ../bin/rd53a_MonitorMUX /dev/ttyUSB0 5 ${SSL_ChipName_SCC_A} configs/rd53a_test.json >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr . | tail -1`
mv ${folderToSave}/*.dat ${CL_ScansPath}/${FullScan_TS}/MUX/
cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/MUX/rd53a_MonitorMUX.log
IRef_Value=`awk '/0 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_CurrentMonitorVmV_MonitorImonMux.dat | sed -n 1p`
Temperature_Sensor_1_Value=`awk '/3 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
Temperature_Sensor_2_Value=`awk '/5 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
Temperature_Sensor_3_Value=`awk '/15 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
Temperature_Sensor_4_Value=`awk '/7 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
Radiation_Sensor_1_Value=`awk '/4 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
Radiation_Sensor_2_Value=`awk '/6 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
Radiation_Sensor_3_Value=`awk '/14 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
Radiation_Sensor_4_Value=`awk '/8 /{print $NF}' ${CL_ScansPath}/${FullScan_TS}/MUX/*_VoltMonitor_MonitorVmonMux.dat | sed -n 1p`
insertChipData="UPDATE ${CL_ChipTable} SET Temperature_Sensor_1 = ${Temperature_Sensor_1_Value}, Temperature_Sensor_2 = ${Temperature_Sensor_2_Value}, Temperature_Sensor_3 = ${Temperature_Sensor_3_Value}, Temperature_Sensor_4 = ${Temperature_Sensor_4_Value}, Radiation_Sensor_1 = ${Radiation_Sensor_1_Value}, Radiation_Sensor_2 = ${Radiation_Sensor_2_Value}, Radiation_Sensor_3 = ${Radiation_Sensor_3_Value}, Radiation_Sensor_4 = ${Radiation_Sensor_4_Value}, IRef = ${IRef_Value} "
insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

#run Ring Oscilators and save results
timeout 10m ../bin/rd53a_test_ringosc -i 20 -d 6 -r 10 -o ${CL_ScansPath}/${FullScan_TS}/ring_osc.log -c configs/rd53a_test.json >& ${CL_RollingScanLogs}/last.log
cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_ringosc.log
Ring_Oscillators_1_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 5p`
Ring_Oscillators_2_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 9p`
Ring_Oscillators_3_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 13p`
Ring_Oscillators_4_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 17p`
Ring_Oscillators_5_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 7p`
Ring_Oscillators_6_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 11p`
Ring_Oscillators_7_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 15p`
Ring_Oscillators_8_Value=`less ${CL_ScansPath}/${FullScan_TS}/ring_osc.log | tail -1 | awk '{for (i=1; i<=NF; i++) print $i}' | sed -n 19p`
insertChipData="UPDATE ${CL_ChipTable} SET Ring_Oscillators_1 = ${Ring_Oscillators_1_Value}, Ring_Oscillators_2 = ${Ring_Oscillators_2_Value}, Ring_Oscillators_3 = ${Ring_Oscillators_3_Value}, Ring_Oscillators_4 = ${Ring_Oscillators_4_Value}, Ring_Oscillators_5 = ${Ring_Oscillators_5_Value}, Ring_Oscillators_6 = ${Ring_Oscillators_6_Value}, Ring_Oscillators_7 = ${Ring_Oscillators_7_Value}, Ring_Oscillators_8 = ${Ring_Oscillators_8_Value} "
insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"

#####################################################################################################################################################
#Run RingOscilators scan for different values of Vddd
sh ${BashScripts}/cron_RingOscilatorScan.sh
#####################################################################################################################################################

#run noise scan and save results
timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_noisescan.json -p -m 0  >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr data/ | tail -1`
cp -r data/${folderToSave} ${CL_ScansPath}/${FullScan_TS}/
cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_noisescan.log
# TODO: calculate relevant quantities
# insertChipData="UPDATE ${CL_ChipTable} SET ..."
# insertChipData="${insertChipData} WHERE TS = ${FullScan_TS}"
# mysql -h ${host} -u ${LogUser} -p${LogPassword} -e "use ${Database}; ${insertChipData};"
cp data/${folderToSave}/${SSL_ChipName_SCC_A}_NoiseMask.png ${WebHome}/Figures/NoiseMask_SCC_A.png

## Finalization

#release lock file
releaseYarrLock

#update Web page
${BashScripts}/cron_WPG.sh

#!/bin/sh
# Backup database content

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

LogTS "Backing up database"
requestArduinoLock
mysqldump --skip-add-locks -u ${LogUser} -p${LogPassword} ${Database} >& ${BACKUP_FolderDB}/slipper.db
releaseArduinoLock
ret=$?
if [ $ret -ne 0 ]; then
    LogTS "ERROR in dumping SQL database for backup."
    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 3 \
	   --message "${progName} ERROR in dumping SQL database for backup" --db 
fi
LogTS "Zipping backup"
mv ${BACKUP_FolderDB}/slipper.db.gz ${BACKUP_FolderDB}/slipper.db.bak.gz
gzip ${BACKUP_FolderDB}/slipper.db
LogTS "Backup done."

#!/bin/bash
# Keep chip busy with a simple noise scan and ring-osclillators running with global-pulse.

## Init

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

while true; do    

    # request YARR lock, default timeout
    requestYarrLock
    if [ $? -ne 0 ]; then
	#timed out or other general error
	python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 2 \
	       --message "${progName} failed requesting Yarr lock." --db
	exit 1
    fi

    ## Run
    cd ${CL_TharPath}/run

    #save results qwith a consistent timestamp
    TS=`date +%s`
    LogTS "ChipLoop TS"
    
    #timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
    #timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_analogGPscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log 
    #noise scan with global pulse for ring oscilators (either we run noiseGPscan or analogGPscan which also has the global pulse for the ring oscilators
    timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_noiseGPscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log

    #release lock file
    releaseYarrLock    

    #temporary save each log file
    cp ${CL_RollingScanLogs}/last.log ${CL_RollingScanLogs}/${TS}.log
    
    #save temperature and currents
    #${BashScripts}/cron_Arduino_IO.sh ${TS} 

    #check PS status and take actions if necessary
    requestUSBLock
    V_1=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 get-voltage`
    V_1_on=`echo "${V_1} > 0.1" | bc`
    V_2=`../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 get-voltage`
    V_2_on=`echo "${V_2} > 0.1" | bc`
    if [[ "${V_1_on}" -eq 0 ]] && [[ "${V_2_on}" -eq 0 ]]; then
	#PS went off. check last time it happened
	lastPSActionTS=`tail -1 .lastPSAction`
	[[ -z "${lastPSActionTS}" ]] && lastPSActionTS=0	
	lastPSActionInterval=$(( TS - lastPSActionTS ))

	#if more than 1hr ago, try to power it on again
	#and save now as last time this was powered on	
	if [[ ${lastPSActionInterval} -ge 1800 ]]; then
	    ../bin/rigol_ctrl --port ${SSL_PS_1} --channel 2 power-on 1.85 2.0
	    sleep 1
	    ../bin/rigol_ctrl --port ${SSL_PS_1} --channel 1 power-on 1.85 2.0
	    echo "${TS}" >> .lastPSAction
	    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 1 --timestamp ${TS} \
		   --message "Powered ON PS" --db
	else
	    echo "${TS}" >> .PSOffNoAction
	fi	    	
    fi
    
    V_3=`../bin/rigol_ctrl --port ${SSL_PS_2} --channel 3 get-voltage`
    V_3_on=`echo "${V_3} > 0.1" | bc`
    if [[ "${V_3_on}" -eq 0 ]]; then
	#PS went off. check last time it happened
	lastPS3ActionTS=`tail -1 .lastPS3Action`
	[[ -z "${lastPS3ActionTS}" ]] && lastPS3ActionTS=0	
	lastPS3ActionInterval=$(( TS - lastPS3ActionTS ))
	
	#if more than 1hr ago, try to power it on again
	#and save now as last time this was powered on	
	if [[ ${lastPS3ActionInterval} -ge 3600 ]]; then
	    ../bin/rigol_ctrl --port ${SSL_PS_2} --channel 3 power-on 3.3 0.2
	    echo "${TS}" >> .lastPS3Action
	    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 1 --timestamp ${TS} \
		   --message "Powered ON ADC PS" --db
	else
	    echo "${TS}" >> .PS3OffNoAction
	fi
    fi
    releaseUSBLock

    #sleeps one second before starting over
    #gives more time for other programs to request lock, if needed
    sleep 1

done

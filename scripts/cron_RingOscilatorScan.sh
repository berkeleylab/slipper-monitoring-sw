#!/bin/bash
# Perform a ring oscilator scan for a different values of Vddd

## Init

baseDir=`dirname $0`
progName=`basename $0`
source $baseDir/config.sh

# request YARR lock, default timeout
###########################################################################
## UNCOMMENT FOR STANDALONE USAGE
###########################################################################
#requestYarrLock
#if [ $? -ne 0 ]; then
#    #timed out or other general error
#    python ${PythonScripts}/Log_Service.py ${CommonCONFIG} --source 3 --type 2 \
#	   --message "${progName} failed requesting Yarr lock."
#    exit 1
#fi

## Run
cd ${CL_TharPath}/run

#save results qwith a consistent timestamp
RingOscillatorScan_TS=`date +%s`
LogTS "RingOscillatorScan TS"
#mkdir ${CL_ScansPath}/${FullScan_TS}

FullScan_TS=`ls -1tr ${CL_ScansPath} | tail -1`

cp configs/rd53a_test.json configs/rd53a_test0.json
cp configs/rd53a_test.json configs/rd53a_test1.json
cp configs/rd53a_test.json configs/rd53a_test2.json
cp configs/rd53a_test.json configs/rd53a_test3.json
value=`grep SldoDigitalTrim configs/rd53a_test.json | grep -o '[0-9]*'`
sed -i "s/\"SldoDigitalTrim\": ${value}/\"SldoDigitalTrim\": $[$value-1]/g" configs/rd53a_test0.json
sed -i "s/\"SldoDigitalTrim\": ${value}/\"SldoDigitalTrim\": $[$value+1]/g" configs/rd53a_test1.json
sed -i "s/\"SldoDigitalTrim\": ${value}/\"SldoDigitalTrim\": $[$value+2]/g" configs/rd53a_test2.json
sed -i "s/\"SldoDigitalTrim\": ${value}/\"SldoDigitalTrim\": $[$value+3]/g" configs/rd53a_test3.json

timeout 10m ../bin/rd53a_test_ringosc -i 20 -d 6 -r 10 -o ${CL_ScansPath}/${FullScan_TS}/ring_osc.log -c configs/rd53a_test0.json >& ${CL_RollingScanLogs}/last.log
timeout 10m python ${PythonScripts}/Arduino_IO.py --fileName ${CL_ScansPath}/${FullScan_TS}/arduino0.dat --table ''
timeout 10m ../bin/rd53a_MonitorMUXRO /dev/ttyUSB0 5 ${SSL_ChipName_SCC_A} configs/rd53a_test0.json >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr . | tail -1`
mv ${folderToSave}/*.dat ${CL_ScansPath}/${FullScan_TS}/MUX/
#timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
#cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_digitalscan_after0.log

timeout 10m ../bin/rd53a_test_ringosc -i 20 -d 6 -r 10 -o ${CL_ScansPath}/${FullScan_TS}/ring_osc.log -c configs/rd53a_test.json >& ${CL_RollingScanLogs}/last.log
timeout 10m python ${PythonScripts}/Arduino_IO.py --fileName ${CL_ScansPath}/${FullScan_TS}/arduino.dat --table ''
timeout 10m ../bin/rd53a_MonitorMUXRO /dev/ttyUSB0 5 ${SSL_ChipName_SCC_A} configs/rd53a_test.json >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr . | tail -1`
mv ${folderToSave}/*.dat ${CL_ScansPath}/${FullScan_TS}/MUX/
#timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
#cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_digitalscan_after.log

timeout 10m ../bin/rd53a_test_ringosc -i 20 -d 6 -r 10 -o ${CL_ScansPath}/${FullScan_TS}/ring_osc.log -c configs/rd53a_test1.json >& ${CL_RollingScanLogs}/last.log
timeout 10m python ${PythonScripts}/Arduino_IO.py --fileName ${CL_ScansPath}/${FullScan_TS}/arduino1.dat --table ''
timeout 10m ../bin/rd53a_MonitorMUXRO /dev/ttyUSB0 5 ${SSL_ChipName_SCC_A} configs/rd53a_test1.json >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr . | tail -1`
mv ${folderToSave}/*.dat ${CL_ScansPath}/${FullScan_TS}/MUX/
#timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
#cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_digitalscan_after1.log

timeout 10m ../bin/rd53a_test_ringosc -i 20 -d 6 -r 10 -o ${CL_ScansPath}/${FullScan_TS}/ring_osc.log -c configs/rd53a_test2.json >& ${CL_RollingScanLogs}/last.log
timeout 10m python ${PythonScripts}/Arduino_IO.py --fileName ${CL_ScansPath}/${FullScan_TS}/arduino2.dat --table ''
timeout 10m ../bin/rd53a_MonitorMUXRO /dev/ttyUSB0 5 ${SSL_ChipName_SCC_A} configs/rd53a_test2.json >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr . | tail -1`
mv ${folderToSave}/*.dat ${CL_ScansPath}/${FullScan_TS}/MUX/
#timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
#cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_digitalscan_after2.log

timeout 10m ../bin/rd53a_test_ringosc -i 20 -d 6 -r 10 -o ${CL_ScansPath}/${FullScan_TS}/ring_osc.log -c configs/rd53a_test3.json >& ${CL_RollingScanLogs}/last.log
timeout 10m python ${PythonScripts}/Arduino_IO.py --fileName ${CL_ScansPath}/${FullScan_TS}/arduino3.dat --table ''
timeout 10m ../bin/rd53a_MonitorMUXRO /dev/ttyUSB0 5 ${SSL_ChipName_SCC_A} configs/rd53a_test3.json >& ${CL_RollingScanLogs}/last.log
folderToSave=`ls -1tr . | tail -1`
mv ${folderToSave}/*.dat ${CL_ScansPath}/${FullScan_TS}/MUX/
#timeout 10m ../bin/scanConsole -r configs/controller/specCfg.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p -m 0 >& ${CL_RollingScanLogs}/last.log
#cp ${CL_RollingScanLogs}/last.log ${CL_ScansPath}/${FullScan_TS}/std_digitalscan_after3.log


## Finalization

#release lock file
###########################################################################
## UNCOMMENT FOR STANDALONE USAGE
###########################################################################
#releaseYarrLock

#update Web page
${BashScripts}/cron_WPG.sh

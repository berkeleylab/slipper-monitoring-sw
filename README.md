# Slipper, SLow Irradiation of Phase-2 PixEl Readout

## Project Description

![Image](WebPage/Gallery/Logic_Demo.svg)

This software manages the collection of data and its monitoring through a web page for the slipper project. It uses a database to collect information and
data from the chip is read and saved through a set of dedicated scripts.

The main components are:
- code and interface with arduino
- scripts to interface with YARR to read chip data and store it in the DB at regular intervals
- code to monitor the system and take actions in case any pre-configured set of conditions happen
- scripts to generate dynamically web pages to display monitored data

The monitoring web page is live at http://http://www-atlas.lbl.gov/~slipper/

## Installation and configuration
 
A detailed setup guide is provided separately [here](docs/First_Launch_Instructions.md).

The configuration for all the programs is maintained in a single config file that lives in `scripts/config.sh`.

### Crontab setup

Scripts should be arranged to be executed automatically through a crontab. The file `slipper.crontab` gives a starting point example.
See also here](docs/First_Launch_Instructions.md) for further information.

## Components descrription and notes

### Log service
Provides log capabilities. It also keep tracks of all known `source` programs and their name/sourceID. 

### Arduino setup
The Arduino code lives in `Arduino_IO_board/`. Some technical notes are available [here](docs/Arduino_Configurations.md).

The program `python/Arduino_IO.py` interfaces with the Arduino and stores data into the database.

### Web page generator
Generate web page for monitoring based on data in DB.
The main program is a shell script: `scripts/cron_WPG.sh`.

The data is collected from the DB and other sources by the python program `python/WebPage_FetchData.py`.

The output is stored in a dictionary and then to file for the substitution.
Template HTML files can be found in the `Raw_HTML/` folder and variables to be substituted are in the format:
```
#__VariableName__#
```

where `VariableName` is the name in the python dictionary to be filled by the python program for substitution.

## Data Analysis 

The program `python/DB_To_Root.py` convert the database into a ROOT file.

Several analysis macros then can be used to make plots and are found in the `macros/` folder.

For creating a new macro, copy the file `ChipNtuple.C` into a new file and modify the `Loop()` function.

## TODO
- Retry scan up to N times if the avg occupancy is low before accepting it. It it's a transient error make a Log entry with Vddd and Vdda values.
- Test and commission fixed links (udev rules) for PS and Arduino
- The noise, threshold and tot plots are updated to the webpage after each scan, it should be only after tuning.

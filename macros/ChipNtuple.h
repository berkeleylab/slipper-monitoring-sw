//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Oct 18 21:54:23 2018 by ROOT version 6.14/04
// from TTree Chip/Chip_Data table
// found on file: slipper-data.root
//////////////////////////////////////////////////////////

#ifndef ChipNtuple_h
#define ChipNtuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ChipNtuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           Data_ID;
   Int_t           Data_TS;
   Int_t           Data_Chip_ID;
   Int_t           Data_Source_Position;
   Int_t           Data_Scan_Type;
   Float_t         Data_Temperature_Sensor_1;
   Float_t         Data_Temperature_Sensor_2;
   Float_t         Data_Temperature_Sensor_3;
   Float_t         Data_Temperature_Sensor_4;
   Float_t         Data_Radiation_Sensor_1;
   Float_t         Data_Radiation_Sensor_2;
   Float_t         Data_Radiation_Sensor_3;
   Float_t         Data_Radiation_Sensor_4;
   Float_t         Data_IRef;
   Float_t         Data_Ring_Oscillators_1;
   Float_t         Data_Ring_Oscillators_2;
   Float_t         Data_Ring_Oscillators_3;
   Float_t         Data_Ring_Oscillators_4;
   Float_t         Data_Ring_Oscillators_5;
   Float_t         Data_Ring_Oscillators_6;
   Float_t         Data_Ring_Oscillators_7;
   Float_t         Data_Ring_Oscillators_8;
   Float_t         Data_DigiScan_Average_Occupancy_FE_1;
   Float_t         Data_AnalogScan_Average_Occupancy_FE_1;
   Float_t         Data_ThrScan_Mean_FE_1;
   Float_t         Data_ThrScan_Sigma_FE_1;
   Float_t         Data_ThrScan_Noise_Mean_FE_1;
   Float_t         Data_ThrScan_Noise_Sigma_FE_1;
   Float_t         Data_ToTScan_Mean_FE_1;
   Float_t         Data_DigiScan_Average_Occupancy_FE_2;
   Float_t         Data_AnalogScan_Average_Occupancy_FE_2;
   Float_t         Data_ThrScan_Mean_FE_2;
   Float_t         Data_ThrScan_Sigma_FE_2;
   Float_t         Data_ThrScan_Noise_Mean_FE_2;
   Float_t         Data_ThrScan_Noise_Sigma_FE_2;
   Float_t         Data_ToTScan_Mean_FE_2;
   Float_t         Data_DigiScan_Average_Occupancy_FE_3;
   Float_t         Data_AnalogScan_Average_Occupancy_FE_3;
   Float_t         Data_ThrScan_Mean_FE_3;
   Float_t         Data_ThrScan_Sigma_FE_3;
   Float_t         Data_ThrScan_Noise_Mean_FE_3;
   Float_t         Data_ThrScan_Noise_Sigma_FE_3;
   Float_t         Data_ToTScan_Mean_FE_3;
   Float_t         Data_VCal_High_Left_Offset;
   Float_t         Data_VCal_High_Left_Slope;
   Float_t         Data_VCal_High_Right_Offset;
   Float_t         Data_VCal_High_Right_Slope;
   Float_t         Data_VCal_Medium_Left_Offset;
   Float_t         Data_VCal_Medium_Left_Slope;
   Float_t         Data_VCal_Medium_Right_Offset;
   Float_t         Data_VCal_Medium_Right_Slope;
   Int_t           Arduino_TS;
   Float_t         Arduino_Env_Temp;
   Float_t         Arduino_Env_Humidity;
   Float_t         Arduino_SCC_A_Vddd;
   Float_t         Arduino_SCC_A_Vdda;
   Float_t         Arduino_SCC_A_VRefd;
   Float_t         Arduino_SCC_A_VRefa;
   Float_t         Arduino_SCC_A_NTC;
   Float_t         Arduino_SCC_A_Digital_I;
   Float_t         Arduino_SCC_A_Analog_I;
   Float_t         Arduino_SCC_B_Vddd;
   Float_t         Arduino_SCC_B_Vdda;
   Float_t         Arduino_SCC_B_VRefd;
   Float_t         Arduino_SCC_B_VRefa;
   Float_t         Arduino_SCC_B_NTC;
   Float_t         Arduino_SCC_B_Current;
   Float_t         Arduino_SCC_B_Digital_I;
   Float_t         Arduino_SCC_B_Analog_I;
   Float_t         Arduino_SCC_C_Vddd;
   Float_t         Arduino_SCC_C_Vdda;
   Float_t         Arduino_SCC_C_VRefd;
   Float_t         Arduino_SCC_C_VRefa;
   Float_t         Arduino_SCC_C_NTC;
   Float_t         Arduino_SCC_C_Digital_I;
   Float_t         Arduino_SCC_C_Analog_I;

   // List of branches
   TBranch        *b_Data;   //!
   TBranch        *b_Arduino;   //!

   ChipNtuple(TTree *tree=0);
   virtual ~ChipNtuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ChipNtuple_cxx
ChipNtuple::ChipNtuple(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("slipper-data.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("slipper-data.root");
      }
      f->GetObject("Chip",tree);

   }
   Init(tree);
}

ChipNtuple::~ChipNtuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ChipNtuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ChipNtuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ChipNtuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Data", &Data_ID, &b_Data);
   fChain->SetBranchAddress("Arduino", &Arduino_TS, &b_Arduino);
   Notify();
}

Bool_t ChipNtuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ChipNtuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ChipNtuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ChipNtuple_cxx

#define ChipNtuple_cxx
#include "ChipNtuple.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void ChipNtuple::Loop()
{
//   In a ROOT session, you can do:
//      root> .L ChipNtuple.C
//      root> ChipNtuple t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      
   }
}

#!/usr/bin/env python
##
## Description:
## Code to query data from Arduino and store it to DB
##
## Authors:
##  Yuxuan Zhang
##  Simone Pagan Griso
##  Aleksandra Dimitrievska
## 

import sys
import argparse
import serial
import time
import signal
import MySQLdb
from array import array
from datetime import datetime
from time import sleep

import Log_Service
from Log_Service import Log
from Log_Service import Log_DB
from Log_Service import Log_In_Key

Show_All_Possible_Error=False
Enable_Arduino_Debug=False
Debug=False
# || Show_All_Possible_Error

class data_unit:
  flag=''
  val=0.0
  table_name=''
  def set_flag(self,def_flag):
    self.flag=def_flag
  def find_table_name(self,InputExpect_str):
    p_str=InputExpect_str.find('$'+self.flag+'@')
    if (self.flag!='' and p_str!=-1): 
      Target_str=InputExpect_str[p_str+len(self.flag)+2:]
      Target_str=Target_str[:Target_str.find('$')]
      self.table_name=Target_str
  def convert_float(self,value_str):
    minus_sign_flag=1
    if(value_str[0]=='-'):
      minus_sign_flag=-1
      value_str=value_str[1:]
    while(len(value_str)>0 and value_str[0]>='0' and value_str[0]<='9'):
      self.val*=10
      self.val+=char_to_int(value_str[0])
      value_str=value_str[1:]
    if(value_str[0]!='.'):
      Log(110,'Invalid Syntax#'+value_str+'#',"Arduino_IO")
    else:
      _pow=1;
      value_str=value_str[1:]
      while(len(value_str)>0 and value_str[0]>='0' and value_str[0]<='9'):
        _pow*=0.1
        self.val+=_pow*char_to_int(value_str[0])
        value_str=value_str[1:]
      if(len(value_str)>0):
        Log(110,'Invalid Syntax>#'+value_str+'#',"Arduino_IO")
    self.val*=minus_sign_flag;

def char_to_int(buff):
  if buff=='0': return 0
  if buff=='1': return 1
  if buff=='2': return 2
  if buff=='3': return 3
  if buff=='4': return 4
  if buff=='5': return 5
  if buff=='6': return 6
  if buff=='7': return 7
  if buff=='8': return 8
  if buff=='9': return 9
  return 0

def fetch_data(Port, baudrate, time_out, timestamp, fileName, Host, User, Password, Database, Table, InputExpect):
  #Start Database Connection
  if Debug:
    Log(Log_Service.DEBUG, 'Connecting to Database using Host:'+Host+' User:'+User, "Arduino_IO")
  try:
    db = MySQLdb.connect(Host, User, Password)
    if Show_All_Possible_Error: raise Exception("Not a real Exception")
  except Exception, e:
    Log_DB(101,"Database Initialization Failed#"+Host+" "+User+" "+Password + "#(Abort): " + str(e),"Arduino_IO");
    if(Show_All_Possible_Error==False):sys.exit(1)
  #Start Serial Connection
  try:
    serial_port = serial.Serial(Port, baudrate, timeout=time_out)
    if Show_All_Possible_Error: raise Exception("Not a real Exception")
  except:
    Log_DB(102,'Serial Port Not Responding#'+Port+'#(Abort)',"Arduino_IO")
    if(Show_All_Possible_Error==False):sys.exit(1)
  if Debug:
    Log(Log_Service.DEBUG,"Serial Connection established","Arduino_IO")
  #Fetch RAW
  if Debug and Enable_Arduino_Debug:
    RAW_Data_str=''
    timeout_count=0
    while(RAW_Data_str=='' and timeout_count<10):
      serial_port.writelines("<DEBUG_ON>")
      time.sleep(0.1)
      RAW_Data_str=serial_port.readline()
      timeout_count+=1
    while(RAW_Data_str!='' and RAW_Data_str.count('#')>=2):
      RAW_Data_str=RAW_Data_str[RAW_Data_str.find('#')+1:]
      if Debug:
        Log(Log_Service.DEBUG, RAW_Data_str[:RAW_Data_str.find('#')], "Arduino_IO")
      RAW_Data_str=RAW_Data_str[RAW_Data_str.find('#'):]
  RAW_Data_str=''
  timeout_count=0
  while(RAW_Data_str=='' and timeout_count<10):
    serial_port.writelines("<DATA>")
    time.sleep(0.1)
    RAW_Data_str=serial_port.readline()
    timeout_count+=1  
  #Pick out error messages
  if Debug:
    Log(Log_Service.DEBUG, RAW_Data_str, "Arduino_IO")
  if (RAW_Data_str.count('#')%2==1 or Show_All_Possible_Error):
    Log_DB(103, 'INVALID Serial INPUT#'+RAW_Data_str+'#(Abort)',"Arduino_IO")
    if(Show_All_Possible_Error==False):sys.exit(1)
  while(RAW_Data_str.find('#')!=-1):
    err_str=RAW_Data_str[RAW_Data_str.find('#')+1:]
    err_str=err_str[:err_str.find('#')]
    if Debug:
      Log(Log_Service.DEBUG, err_str+"(PROCEED)","Arduino_IO")
    temp_str=RAW_Data_str[:RAW_Data_str.find('#')]
    RAW_Data_str=RAW_Data_str[RAW_Data_str.find('#')+1:]
    RAW_Data_str=RAW_Data_str[RAW_Data_str.find('#')+1:]
    RAW_Data_str=temp_str+RAW_Data_str
  #Pick out values
  if (RAW_Data_str.count('<')!=1 or RAW_Data_str.count('>')!=1 or Show_All_Possible_Error):
    Log_DB(103,'INVALID Serial INPUT#'+RAW_Data_str+'#(Ignore)', "Arduino_IO")
    if(Show_All_Possible_Error==False):sys.exit(1)
  RAW_Data_str=RAW_Data_str[RAW_Data_str.find('<')+1:]
  RAW_Data_str=RAW_Data_str[:RAW_Data_str.find('>')]
  if Debug:
    Log(Log_Service.DEBUG, "NOW HANDLING : "+RAW_Data_str, "Arduino_IO")
  keyValsPairs = {} # store retrieved data from Arduino for DB insertion 
  while(RAW_Data_str.count('$')>1):
    DATA=data_unit()
    RAW_Data_str=RAW_Data_str[RAW_Data_str.find('$')+1:]
    Current_Data_str=RAW_Data_str[:RAW_Data_str.find('$')]
    RAW_Data_str=RAW_Data_str[RAW_Data_str.find('$'):]
    if Debug:
      Log(Log_Service.DEBUG, "NOW HANDLING : "+RAW_Data_str, "Arduino_IO")
    if(Current_Data_str.count('%')!=1 or Show_All_Possible_Error):
      Log_DB(10,'Skipping invalid Data Record#'+Current_Data_str+'#(PROCEED)',"Arduino_IO")
    if(Current_Data_str.count('%')==1):#else:
      DATA.set_flag(Current_Data_str[:Current_Data_str.find('%')])
      DATA.convert_float(Current_Data_str[Current_Data_str.find('%')+1:])
      DATA.find_table_name(InputExpect)
      if(DATA.table_name=='' or Show_All_Possible_Error):
        Log_DB(11,'Invalid Data Tag#'+Current_Data_str[:Current_Data_str.find('%')]+'#(PROCEED)',"Arduino_IO")
        #msg_type,msg_string,error_ID=0,priority=0,msg_source
      if (DATA.table_name!=''):#else:
        keyValsPairs[DATA.table_name] = DATA.val

  #Now upload data to DB
  if fileName:
    #print DATA.val
    try:
      outtxt = open(fileName, "w")
      outstr=""
      for k in keyValsPairs:
        outstr = outstr + "%s = %s\n" % (str(k), str(keyValsPairs[k]))
      outtxt.write(outstr)
      outtxt.close()
    except:
      Log_DB(105, "Failed to write data into output file: %s" % (fileName))

  if Table:
    try:
      cursor = db.cursor()
      cursor.execute("use "+Database)
      INSERT_CMD="INSERT INTO "+Table+" (TS"
      for k in keyValsPairs:
        INSERT_CMD = INSERT_CMD + ", " + k
      INSERT_CMD = INSERT_CMD + ") VALUES("
      INSERT_CMD = INSERT_CMD + '%d' % timestamp
      for k in keyValsPairs:
        INSERT_CMD = INSERT_CMD + ", %.4f" % (keyValsPairs[k])
      INSERT_CMD = INSERT_CMD + ")"
      if Debug:
        Log(Log_Service.DEBUG, 'MySQL_CMD >> '+INSERT_CMD, "Arduino_IO");
      cursor.execute(INSERT_CMD)
      db.commit()
      if Show_All_Possible_Error: raise Exception("Not a real Exception")
    except:
      Log_DB(104,"Failed_To_Commit_CMD#"+INSERT_CMD+"#(Abort)","Arduino_IO");
      if(Show_All_Possible_Error==False):
        db.close()
        sys.exit(1)
    db.close()
  if Debug:
    Log(Log_Service.DEBUG,"Debug_Run_Succeded TS%d"%timestamp,"Ardiono_IO")
    
if __name__ == '__main__':
    
  parser = argparse.ArgumentParser(description='Read serial data and record temperature data.', usage='%(prog)s [options]', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--port',      default='/dev/ttyArd0',  help='input serial port')
  parser.add_argument('--baudrate',    type=int,       default=9600,  help='baud rate')
  parser.add_argument('--timeout',        type=float,      default=1,    help='timeout for serial reading [s]')
  parser.add_argument('--timestamp',      type=int,      default=0,      help='timestamp')
  parser.add_argument('--InputExpect',    default='$T@Env_Temp$H@Env_Humidity$NTC@SCC_A_NTC$VDDA@SCC_A_Vdda$VDDD@SCC_A_Vddd$VrefA@SCC_A_VRefa$VrefD@SCC_A_VRefd$',help='Sign of datatype and corresponding column name')

  parser.add_argument('--fileName',      default='',  help='Save the data in a file')
  parser.add_argument('--host',      default='localhost',  help='Database Host') 
  parser.add_argument('--database',    default='slipper_test', help='Name of Database')
  parser.add_argument('--user',      default='slipper', help='Database username')
  parser.add_argument('--password',    default='pxKr_LOG',  help='Database password')
  parser.add_argument('--table',          default='ARDUINO_IO', help='Name of Target Table')  

  parser.add_argument('--LogFile',      default='ArduinoIO.log',      help='Target Version Control TXT file')
  parser.add_argument('--LogTable',     default='Log',         help='Target Error Table')
  parser.add_argument('--Debug',          default=False,        action='store_true', help='Do not print to screen.')
  
  parser.add_argument('--Show_All_Possible_Error',          default=False,        action='store_true', help='Do not print to screen.')
  
  args = parser.parse_args(sys.argv[1:])
  Debug=args.Debug

  Show_All_Possible_Error=args.Show_All_Possible_Error

  # Setup logging information
  #Log_Service.Debug=Debug
  Log_Service.log_file_route=args.LogFile
  Log_Service.L_Key=Log_In_Key(args.host,args.user,args.password,args.database,args.LogTable)

  if args.timestamp==0: args.timestamp=(int)(time.time())
  fetch_data(args.port, args.baudrate, float(args.timeout), args.timestamp, args.fileName, args.host, args.user, args.password, args.database, args.table, args.InputExpect)
  sys.exit(0)
  

#!/usr/bin/env python
##
## Description:
## Convert Database into ROOT file.
## Optionally execute extra operations to the stored data value to compute new variables.
##
## Authors:
##  Simone Pagan Griso
##  Aleksandra Dimitrievska
## 
import sys
import time
import os
import argparse
import MySQLdb
import random
from datetime import datetime
import ROOT
from ROOT import TTree
from ROOT import TFile
from ROOT import gROOT

import Log_Service
from Log_Service import Log
from Log_Service import Log_DB
from Log_Service import Log_In_Key

###### SETTINGS ###############
Debug=False

###### UTILITIES ###############
#returns a float from a DB FLOAT field
def dbToFloat(x):
    try:
        return float(x)
    except:
        return 0.0

###### ARDUINO_IO ###############
#List of output variables
#Note: twice, in ROOT TBranch format and as a C structure (annoying but necessary)
arduinoVars='TS/I:Env_Temp/F:Env_Humidity/F:SCC_A_Vddd/F:SCC_A_Vdda/F:SCC_A_VRefd/F:SCC_A_VRefa/F:SCC_A_NTC/F:SCC_A_Digital_I/F:SCC_A_Analog_I/F:SCC_B_Vddd/F:SCC_B_Vdda/F:SCC_B_VRefd/F:SCC_B_VRefa/F:SCC_B_NTC/F:SCC_B_Current/F:SCC_B_Digital_I/F:SCC_B_Analog_I/F:SCC_C_Vddd/F:SCC_C_Vdda/F:SCC_C_VRefd/F:SCC_C_VRefa/F:SCC_C_NTC/F:SCC_C_Digital_I/F:SCC_C_Analog_I/F'
gROOT.ProcessLine(
"struct arduino_t {\
    Int_t   TS;\
    Float_t Env_Temp;\
    Float_t Env_Humidity;\
    Float_t SCC_A_Vddd;\
    Float_t SCC_A_Vdda;\
    Float_t SCC_A_VRefd;\
    Float_t SCC_A_VRefa;\
    Float_t SCC_A_NTC;\
    Float_t SCC_A_Digital_I;\
    Float_t SCC_A_Analog_I;\
    Float_t SCC_B_Vddd;\
    Float_t SCC_B_Vdda;\
    Float_t SCC_B_VRefd;\
    Float_t SCC_B_VRefa;\
    Float_t SCC_B_NTC;\
    Float_t SCC_B_Current;\
    Float_t SCC_B_Digital_I;\
    Float_t SCC_B_Analog_I;\
    Float_t SCC_C_Vddd;\
    Float_t SCC_C_Vdda;\
    Float_t SCC_C_VRefd;\
    Float_t SCC_C_VRefa;\
    Float_t SCC_C_NTC;\
    Float_t SCC_C_Digital_I;\
    Float_t SCC_C_Analog_I;\
};" );

def storeArduinoData(dbCursor, filterStr):
    #Select data
    SqlQuery = "SELECT * FROM ARDUINO_IO"
    if (filterStr):
        SqlQuery = SqlQuery + " WHERE " + filterStr
    if (Debug):
        Log(Log_Service.DEBUG, 'Executing query: ' + SqlQuery, 'D2R')
    dbCursor.execute(SqlQuery)
    if (Debug):
        Log(Log_Service.DEBUG, 'Query result:', 'D2R')
        for row in dbCursor:
            Log(Log_Service.DEBUG, str(row), 'D2R')
    
    #Create output branches
    outData = TTree('Arduino', 'Arduino table')
    arduino = ROOT.arduino_t()
    outData.Branch('IO', arduino, arduinoVars)
    
    #Fill data
    for row in dbCursor:
        arduino.TS=int(row[0])
        arduino.Env_Temp=dbToFloat(row[1])
        arduino.Env_Humidity=dbToFloat(row[2])
        arduino.SCC_A_Vddd=dbToFloat(row[3])
        arduino.SCC_A_Vdda=dbToFloat(row[4])
        arduino.SCC_A_VRefd=dbToFloat(row[5])
        arduino.SCC_A_VRefa=dbToFloat(row[6])
        arduino.SCC_A_NTC=dbToFloat(row[7])
        arduino.SCC_A_Digital_I=dbToFloat(row[8])
        arduino.SCC_A_Analog_I=dbToFloat(row[9])
        arduino.SCC_B_Vddd=dbToFloat(row[10])
        arduino.SCC_B_Vdda=dbToFloat(row[11])
        arduino.SCC_B_VRefd=dbToFloat(row[12])
        arduino.SCC_B_VRefa=dbToFloat(row[13])
        arduino.SCC_B_NTC=dbToFloat(row[14])
        arduino.SCC_B_Digital_I=dbToFloat(row[15])
        arduino.SCC_B_Analog_I=dbToFloat(row[16])
        arduino.SCC_C_Vddd=dbToFloat(row[17])
        arduino.SCC_C_Vdda=dbToFloat(row[18])
        arduino.SCC_C_VRefd=dbToFloat(row[19])
        arduino.SCC_C_VRefa=dbToFloat(row[20])
        arduino.SCC_C_NTC=dbToFloat(row[21])
        arduino.SCC_C_Digital_I=dbToFloat(row[22])
        arduino.SCC_C_Analog_I=dbToFloat(row[23])

        outData.Fill()

    #Return TTree
    return outData

###### CHIP_DATA ###############
#Definition of output tree
# Note: left out the Data_Path, since we don't need it in the ROOT file
#       but we can use it to compute extra variables
chipDataVars='ID/I:TS/I:Chip_ID/I:Source_Position/I:Scan_Type/I:Temperature_Sensor_1/F:Temperature_Sensor_2/F:Temperature_Sensor_3/F:Temperature_Sensor_4/F:Radiation_Sensor_1/F:Radiation_Sensor_2/F:Radiation_Sensor_3/F:Radiation_Sensor_4/F:IRef/F:Ring_Oscillators_1/F:Ring_Oscillators_2/F:Ring_Oscillators_3/F:Ring_Oscillators_4/F:Ring_Oscillators_5/F:Ring_Oscillators_6/F:Ring_Oscillators_7/F:Ring_Oscillators_8/F:DigiScan_Average_Occupancy_FE_1/F:AnalogScan_Average_Occupancy_FE_1/F:ThrScan_Mean_FE_1/F:ThrScan_Sigma_FE_1/F:ThrScan_Noise_Mean_FE_1/F:ThrScan_Noise_Sigma_FE_1/F:ToTScan_Mean_FE_1/F:DigiScan_Average_Occupancy_FE_2/F:AnalogScan_Average_Occupancy_FE_2/F:ThrScan_Mean_FE_2/F:ThrScan_Sigma_FE_2/F:ThrScan_Noise_Mean_FE_2/F:ThrScan_Noise_Sigma_FE_2/F:ToTScan_Mean_FE_2/F:DigiScan_Average_Occupancy_FE_3/F:AnalogScan_Average_Occupancy_FE_3/F:ThrScan_Mean_FE_3/F:ThrScan_Sigma_FE_3/F:ThrScan_Noise_Mean_FE_3/F:ThrScan_Noise_Sigma_FE_3/F:ToTScan_Mean_FE_3/F:VCal_High_Left_Offset/F:VCal_High_Left_Slope/F:VCal_High_Right_Offset/F:VCal_High_Right_Slope/F:VCal_Medium_Left_Offset/F:VCal_Medium_Left_Slope/F:VCal_Medium_Right_Offset/F:VCal_Medium_Right_Slope/F'
gROOT.ProcessLine(
"struct chipData_t {\
Int_t   ID;\
Int_t   TS;\
Int_t Chip_ID;\
Int_t Source_Position;\
Int_t Scan_Type;\
Float_t Temperature_Sensor_1;\
Float_t Temperature_Sensor_2;\
Float_t Temperature_Sensor_3;\
Float_t Temperature_Sensor_4;\
Float_t Radiation_Sensor_1;\
Float_t Radiation_Sensor_2;\
Float_t Radiation_Sensor_3;\
Float_t Radiation_Sensor_4;\
Float_t IRef;\
Float_t Ring_Oscillators_1;\
Float_t Ring_Oscillators_2;\
Float_t Ring_Oscillators_3;\
Float_t Ring_Oscillators_4;\
Float_t Ring_Oscillators_5;\
Float_t Ring_Oscillators_6;\
Float_t Ring_Oscillators_7;\
Float_t Ring_Oscillators_8;\
Float_t DigiScan_Average_Occupancy_FE_1;\
Float_t AnalogScan_Average_Occupancy_FE_1;\
Float_t ThrScan_Mean_FE_1;\
Float_t ThrScan_Sigma_FE_1;\
Float_t ThrScan_Noise_Mean_FE_1;\
Float_t ThrScan_Noise_Sigma_FE_1;\
Float_t ToTScan_Mean_FE_1;\
Float_t DigiScan_Average_Occupancy_FE_2;\
Float_t AnalogScan_Average_Occupancy_FE_2;\
Float_t ThrScan_Mean_FE_2;\
Float_t ThrScan_Sigma_FE_2;\
Float_t ThrScan_Noise_Mean_FE_2;\
Float_t ThrScan_Noise_Sigma_FE_2;\
Float_t ToTScan_Mean_FE_2;\
Float_t DigiScan_Average_Occupancy_FE_3;\
Float_t AnalogScan_Average_Occupancy_FE_3;\
Float_t ThrScan_Mean_FE_3;\
Float_t ThrScan_Sigma_FE_3;\
Float_t ThrScan_Noise_Mean_FE_3;\
Float_t ThrScan_Noise_Sigma_FE_3;\
Float_t ToTScan_Mean_FE_3;\
Float_t VCal_High_Left_Offset;\
Float_t VCal_High_Left_Slope;\
Float_t VCal_High_Right_Offset;\
Float_t VCal_High_Right_Slope;\
Float_t VCal_Medium_Left_Offset;\
Float_t VCal_Medium_Left_Slope;\
Float_t VCal_Medium_Right_Offset;\
Float_t VCal_Medium_Right_Slope;\
};" );

def storeChipData(dbCursor, filterStr):
    #Select data from both Chip_Data and ARDUINO_IO with same TS (one entry per Chip_Data entry)
    SqlQuery = "SELECT Chip_Data.*,ARDUINO_IO.* FROM Chip_Data"
    SqlQuery = SqlQuery + " INNER JOIN ARDUINO_IO ON Chip_Data.TS=ARDUINO_IO.TS"
    if (filterStr):
        SqlQuery = SqlQuery + " WHERE " + filterStr
    if (Debug):
        Log(Log_Service.DEBUG, 'Executing query: ' + SqlQuery, 'D2R')
    dbCursor.execute(SqlQuery)
    if (Debug):
        Log(Log_Service.DEBUG, 'Query result:', 'D2R')
        for row in dbCursor:
            Log(Log_Service.DEBUG, str(row), 'D2R')
    
    #Create output branches
    outData = TTree('Chip', 'Chip_Data table')
    chip = ROOT.chipData_t()
    arduino = ROOT.arduino_t()
    outData.Branch('Data', chip, chipDataVars)
    #outData.Branch('Data_Path', AddressOf( chip, 'Data_Path' ), 'Data_Path/C' )
    #outData.Branch('Data', chip.Temperature_Sensor_1, '')
    outData.Branch('Arduino', arduino, arduinoVars)    

    
    #Look over Chip_Data
    numScanPathErrors=0
    for row in dbCursor:
        #Compute custom variables, if any
        scanPath=str(row[5])
        if (not os.path.exists(scanPath)):
            numScanPathErrors = numScanPathErrors+1
            if (numScanPathErrors < 10):
                Log(2, "Scan path does not exist: %s" % scanPath, 'D2R')
            elif (numScanPathErrors == 10+1):
                Log(2, "Maximum number of scanPath non-existent errors reached. Silencing", 'D2R')
        #else ... here you can compute new things...
        
        #Fill output branch
        chip.ID=int(row[0])
        chip.TS=int(row[1])
        chip.Chip_ID=int(row[2])
        chip.Source_Position=int(row[3])
        chip.Scan_Type=int(row[4])
        #chip.Data_Path=row[5]
        chip.Temperature_Sensor_1=dbToFloat(row[6])
        chip.Temperature_Sensor_2=dbToFloat(row[7])
        chip.Temperature_Sensor_3=dbToFloat(row[8])
        chip.Temperature_Sensor_4=dbToFloat(row[9])
        chip.Radiation_Sensor_1=dbToFloat(row[10])
        chip.Radiation_Sensor_2=dbToFloat(row[11])
        chip.Radiation_Sensor_3=dbToFloat(row[12])
        chip.Radiation_Sensor_4=dbToFloat(row[13])
        chip.IRef=dbToFloat(row[14])
        chip.Ring_Oscillators_1=dbToFloat(row[15])
        chip.Ring_Oscillators_2=dbToFloat(row[16])
        chip.Ring_Oscillators_3=dbToFloat(row[17])
        chip.Ring_Oscillators_4=dbToFloat(row[18])
        chip.Ring_Oscillators_5=dbToFloat(row[19])
        chip.Ring_Oscillators_6=dbToFloat(row[20])
        chip.Ring_Oscillators_7=dbToFloat(row[21])
        chip.Ring_Oscillators_8=dbToFloat(row[22])
        chip.DigiScan_Average_Occupancy_FE_1=dbToFloat(row[23])
        chip.AnalogScan_Average_Occupancy_FE_1=dbToFloat(row[24])
        chip.ThrScan_Mean_FE_1=dbToFloat(row[25])
        chip.ThrScan_Sigma_FE_1=dbToFloat(row[26])
        chip.ThrScan_Noise_Mean_FE_1=dbToFloat(row[27])
        chip.ThrScan_Noise_Sigma_FE_1=dbToFloat(row[28])
        chip.ToTScan_Mean_FE_1=dbToFloat(row[29])
        chip.DigiScan_Average_Occupancy_FE_2=dbToFloat(row[30])
        chip.AnalogScan_Average_Occupancy_FE_2=dbToFloat(row[31])
        chip.ThrScan_Mean_FE_2=dbToFloat(row[32])
        chip.ThrScan_Sigma_FE_2=dbToFloat(row[33])
        chip.ThrScan_Noise_Mean_FE_2=dbToFloat(row[34])
        chip.ThrScan_Noise_Sigma_FE_2=dbToFloat(row[35])
        chip.ToTScan_Mean_FE_2=dbToFloat(row[36])
        chip.DigiScan_Average_Occupancy_FE_3=dbToFloat(row[37])
        chip.AnalogScan_Average_Occupancy_FE_3=dbToFloat(row[38])
        chip.ThrScan_Mean_FE_3=dbToFloat(row[39])
        chip.ThrScan_Sigma_FE_3=dbToFloat(row[40])
        chip.ThrScan_Noise_Mean_FE_3=dbToFloat(row[41])
        chip.ThrScan_Noise_Sigma_FE_3=dbToFloat(row[42])
        chip.ToTScan_Mean_FE_3=dbToFloat(row[43])
        chip.VCal_High_Left_Offset=dbToFloat(row[44])
        chip.VCal_High_Left_Slope=dbToFloat(row[45])
        chip.VCal_High_Right_Offset=dbToFloat(row[46])
        chip.VCal_High_Right_Slope=dbToFloat(row[47])
        chip.VCal_Medium_Left_Offset=dbToFloat(row[48])
        chip.VCal_Medium_Left_Slope=dbToFloat(row[49])
        chip.VCal_Medium_Right_Offset=dbToFloat(row[50])
        chip.VCal_Medium_Right_Slope=dbToFloat(row[51])
        arduinoOffsetIndex=52
        arduino.TS=int(row[0+arduinoOffsetIndex])
        arduino.Env_Temp=dbToFloat(row[1+arduinoOffsetIndex])
        arduino.Env_Humidity=dbToFloat(row[2+arduinoOffsetIndex])
        arduino.SCC_A_Vddd=dbToFloat(row[3+arduinoOffsetIndex])
        arduino.SCC_A_Vdda=dbToFloat(row[4+arduinoOffsetIndex])
        arduino.SCC_A_VRefd=dbToFloat(row[5+arduinoOffsetIndex])
        arduino.SCC_A_VRefa=dbToFloat(row[6+arduinoOffsetIndex])
        arduino.SCC_A_NTC=dbToFloat(row[7+arduinoOffsetIndex])
        arduino.SCC_A_Digital_I=dbToFloat(row[8+arduinoOffsetIndex])
        arduino.SCC_A_Analog_I=dbToFloat(row[9+arduinoOffsetIndex])
        arduino.SCC_B_Vddd=dbToFloat(row[10+arduinoOffsetIndex])
        arduino.SCC_B_Vdda=dbToFloat(row[11+arduinoOffsetIndex])
        arduino.SCC_B_VRefd=dbToFloat(row[12+arduinoOffsetIndex])
        arduino.SCC_B_VRefa=dbToFloat(row[13+arduinoOffsetIndex])
        arduino.SCC_B_NTC=dbToFloat(row[14+arduinoOffsetIndex])
        arduino.SCC_B_Digital_I=dbToFloat(row[15+arduinoOffsetIndex])
        arduino.SCC_B_Analog_I=dbToFloat(row[16+arduinoOffsetIndex])
        arduino.SCC_C_Vddd=dbToFloat(row[17+arduinoOffsetIndex])
        arduino.SCC_C_Vdda=dbToFloat(row[18+arduinoOffsetIndex])
        arduino.SCC_C_VRefd=dbToFloat(row[19+arduinoOffsetIndex])
        arduino.SCC_C_VRefa=dbToFloat(row[20+arduinoOffsetIndex])
        arduino.SCC_C_NTC=dbToFloat(row[21+arduinoOffsetIndex])
        arduino.SCC_C_Digital_I=dbToFloat(row[22+arduinoOffsetIndex])
        arduino.SCC_C_Analog_I=dbToFloat(row[23+arduinoOffsetIndex])
        
        outData.Fill()

    #Write summary
    Log(0, "--------", 'D2R')
    Log(0, 'Summary of Chip_Data conversion', 'D2R')
    Log(0, " # non-existing scan paths: %d" % numScanPathErrors, 'D2R')
    Log(0, "--------", 'D2R')

    #Return TTree
    return outData
        

###### MAIN ###############
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Convert DB data into ROOT file.', usage='%(prog)s [options]', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--output', default='slipper-data.root', type=str, help='Output ROOT file name')
    parser.add_argument('--filters', default='', type=str, help='Optional filtering to be used in the mySQL "WHERE" statement, if any. Format: "Table1:filter1;Table2:filter2;..."')
    
    parser.add_argument('--host', default='localhost',  help='Database Host') 
    parser.add_argument('--database', default='slipper', help='Name of Database')
    parser.add_argument('--user', default='slipper', help='Database username')
    parser.add_argument('--password', default='pxKr_LOG',  help='Database password')
    
    parser.add_argument('--LogFile', default='D2R.log',      help='Output log file')
    parser.add_argument('--Debug', default=False,        action='store_true', help='Enable debug logging')
   
    args = parser.parse_args(sys.argv[1:])

    if(Debug):
        Log(Log_Service.DEBUG, "Starting WebPage_FetchData.py", 'D2R')
        
    #Settings
    Debug=args.Debug

    Log_Service.log_file_route=args.LogFile
    Log_Service.L_Key=Log_In_Key(args.host,args.user,args.password,args.database,'')
    
    #Open connection to DB
    if Debug:
        Log(Log_Service.DEBUG, 'Connecting to Database using Host:'+args.host+' User:'+args.user, "D2R")
    try:
        db=MySQLdb.connect(args.host,args.user,args.password)
        dbCursor=db.cursor()
        dbCursor.execute('use '+args.database)
    except Exception, e:
      Log(Log_Service.ERROR, 'Error connecting to DB: ' + str(e), 'D2R')
      sys.exit(1)
                    
    #Parse filters
    filters={}
    filterTables=args.filters.split(';')
    for f in filterTables:
        tabName=f.split(':')[0]
        filterQuery=f.split(':')[1:]
        filters[tabName]=filterQuery

    #Prepare output file
    outRoot = TFile.Open(args.output, 'RECREATE')
        
    #Fetch data from the ARDUINO_IO Table
    sqlFilter=''
    if filters.has_key('ARDUINO_IO'):
        sqlFilter=filters['ARDUINO_IO']
    outArduinoData = storeArduinoData(dbCursor, sqlFilter)
    outRoot.cd()
    outArduinoData.Write()
    
    #Fetch data from the Chip_Data Table
    sqlFilter=''
    if filters.has_key('Chip_Data'):
        sqlFilter=filters['Chip_Data']
    outChipData = storeChipData(dbCursor, sqlFilter)
    outRoot.cd()
    outChipData.Write()
    

    #Clean-up and exit
    db.close()
    outRoot.Close()
    sys.exit(0)

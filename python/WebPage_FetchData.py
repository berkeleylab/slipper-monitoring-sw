#!/usr/bin/env python
##
## Description:
## Fetch data needed for web page generation and store it into an output file with format:
## VAR=VALUE
##
## Note: see README.md for a description; this script is normally not called directly by the user
##       but called within scripts/WebPage_Generator.sh
##
##
## Authors:
##  Simone Pagan Griso
##  Aleksandra Dimitrievska
## 
import sys
import time
import argparse
import MySQLdb
import random
from datetime import datetime

import Log_Service
from Log_Service import Log
from Log_Service import Log_DB
from Log_Service import Log_In_Key

###### SETTINGS ###############
Debug=False


###### UTILITIES ###############

# Fetch the last 'limit' rows, as orderd in descending order by 'orderVar'
# for variable 'varName' in table 'tableName'
def fetchVarListDB(varName, tableName, dbCursor, limit=1, orderVar='TS'):
    SqlQuery = "SELECT %s FROM %s" % (varName, tableName)
    if (orderVar != ''):
        SqlQuery = SqlQuery + " ORDER BY %s DESC" % (orderVar)
    if (limit > 0):
        SqlQuery = SqlQuery + " LIMIT %d" % (limit)
    SqlQuery = SqlQuery + ';'
    if (Debug):
        Log(Log_Service.DEBUG, 'Executing query: ' + SqlQuery, 'WPG')        
    dbCursor.execute(SqlQuery)
    if (Debug):
        Log(Log_Service.DEBUG, 'Query result:', 'WPG')
        for row in dbCursor:
            Log(Log_Service.DEBUG, str(row), 'WPG')
    #return tuple of values (one variable requested only)
    queryResult=[]
    for row in dbCursor:
        queryResult.append(row[0])
    return queryResult

# Fetch last time-stamped value
def fetchLastTSVarDB(varName, tableName, dbCursor):
    out=fetchVarListDB(varName, tableName, dbCursor, 1, 'TS')
    return out[0]

# Fetch the last scan of a given type for variable 'varName'
def fetchLastScanTypeDB(varName, dbCursor, scanType):
    SqlQuery = "SELECT %s FROM Chip_Data" % (varName)
    SqlQuery = SqlQuery + " WHERE Scan_Type = %d" % (int(scanType))
    SqlQuery = SqlQuery + " ORDER BY TS DESC"
    SqlQuery = SqlQuery + " LIMIT 1"
    SqlQuery = SqlQuery + ';'
    if (Debug):
        Log(Log_Service.DEBUG, 'Executing query: ' + SqlQuery, 'WPG')        
    dbCursor.execute(SqlQuery)
    if (Debug):
        Log(Log_Service.DEBUG, 'Query result:', 'WPG')
        for row in dbCursor:
            Log(Log_Service.DEBUG, str(row), 'WPG')
    #return tuple of values (one variable requested only)
    queryResult=[]
    for row in dbCursor:
        queryResult.append(row[0])
    return queryResult[0]

# Format string for HTML display
def formatHTML(t_str):
    t_str=t_str.replace('&','&amp;')
    t_str=t_str.replace('<','&lt;')
    t_str=t_str.replace('>>','&ggt;')
    t_str=t_str.replace('>','&gt;')
    t_str=t_str.replace('&gt;','&gt;<br>')
    t_str=t_str.replace('&ggt;','&gt;&gt;')
    t_str=t_str.replace('"','&quot;')
    t_str=t_str.replace("'",'&apos;')
    t_str=t_str.replace('#','<br>')
    if(t_str[len(t_str)-3:]=='<br>'):t_str=t_str[len(t_str)-3:]
    while (t_str.count('<br><br>')>0) : t_str=t_str.replace('<br><br>','<br>')
    t_str=t_str.replace('$FAKE$ ','')
    return t_str

# Format a numeric vector for output
def formatNumVec(vec):
    outStr=',\\n '.join(vec)
    return outStr

# Format output string with the HTML table code needed.
# Note that all columns need to be passed at once to properly
#  define the needed code. All vectors need to have the same length.
def formatForLogTable(vecID, vecSource, vecType, vecMessage, vecDate):
    idx=0
    outStr=''
    for i in range(0, len(vecID)):
        outStr=outStr + "      <tr class=\"log_table\" id=\"data-row-%d\">\\n" % i
        outStr=outStr + "        <td style=\"line-height:28px;background-color:\#AAAAAA;text-align: center; color: \#fff;\">"
	outStr=outStr + "	  <p>%d</p>\\n" % vecID[i]
	outStr=outStr + "	</td>\\n"
	outStr=outStr + "	<td style=\"line-height:28px;background-color:#4BC2C5;text-align: center; color: #fff;\">\\n"
	outStr=outStr + "	</td>\\n"
	outStr=outStr + "	<td style=\"line-height:28px;background-color:#DDDDDD;text-align: center; color: #000;\">\\n"
	outStr=outStr + "	  <p>%d</p>\\n" % vecSource[i]
	outStr=outStr + "	</td>\\n"
	outStr=outStr + "	<td style=\"line-height:28px;background-color:#EEEEEE;text-align: center; color: #000;\">\\n"
	outStr=outStr + "	  <p>%d</p>\\n" % vecType[i]
	outStr=outStr + "	</td>\\n"
	outStr=outStr + "	<td style=\"line-height:28px;background-color:#DDEEE8;text-align: center; color: #000;\">\\n"
	outStr=outStr + "	  <p>%s</p>\\n" % formatHTML(vecMessage[i])
	outStr=outStr + "	</td>\\n"
	outStr=outStr + "	<td style=\"line-height:28px;background-color:#DDDDDD;text-align: center; color: #000000;\">\\n"
	outStr=outStr + "	  <p>%s</p>\\n" % vecDate[i]
	outStr=outStr + "	</td>\\n"
	outStr=outStr + "      </tr>\\n"
	outStr=outStr + "      <tr style=\"height: 8px\" id=\"data-row-blk-%s\">\\n" % i
	outStr=outStr + "	<td colspan=\"6\" style=\"background-color:rgba(255,255,255,0);text-align:center;\">\\n"
	outStr=outStr + "      </tr>\\n"
        outStr=outStr + "\\n"
    
    return outStr

###### FETCH DATA ###############

## The following function simply fetches all data we may need for the web page
##  and stores it into a dictionary. Customization is more important than
##  generaliry here.
def fetchData(dbCursor):
    outVars={}

    #### General
    outVars['CURRENT_TIMESTAMP']=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    
    #### Table ARDUINO_IO
    maxGraphEntries=5000
    outVars['ArduinoIO.TS']= formatNumVec([ "%d" % (v*1000) for v in fetchVarListDB('TS', 'ARDUINO_IO',dbCursor, maxGraphEntries) ])
    outVars['ArduinoIO.TS_Last']= unicode(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(fetchLastTSVarDB('TS', 'ARDUINO_IO', dbCursor))))
    outVars['ArduinoIO.ET']="%.1f" % fetchLastTSVarDB('Env_Temp', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.ET_D']= formatNumVec([ "%.2f" % v for v in fetchVarListDB('Env_Temp', 'ARDUINO_IO',dbCursor, maxGraphEntries) ])
    outVars['ArduinoIO.EH']="%d" % fetchLastTSVarDB('Env_Humidity', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.EH_D']= formatNumVec([ "%.2f" % v for v in fetchVarListDB('Env_Humidity', 'ARDUINO_IO',dbCursor, maxGraphEntries) ])

    outVars['ArduinoIO.SCC_A_Vdda']="%.2f" % fetchLastTSVarDB('SCC_A_Vdda', 'ARDUINO_IO',dbCursor)    
    outVars['ArduinoIO.SCC_A_Vddd']="%.2f" % fetchLastTSVarDB('SCC_A_Vddd', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_A_Digital_I']="%.3f" % fetchLastTSVarDB('SCC_A_Digital_I', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_A_Analog_I']="%.3f" % fetchLastTSVarDB('SCC_A_Analog_I', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_A_NTC']="%.1f" % fetchLastTSVarDB('SCC_A_NTC', 'ARDUINO_IO',dbCursor)

    outVars['ArduinoIO.SCC_B_Vdda']="%.2f" % fetchLastTSVarDB('SCC_B_Vdda', 'ARDUINO_IO',dbCursor)    
    outVars['ArduinoIO.SCC_B_Vddd']="%.2f" % fetchLastTSVarDB('SCC_B_Vddd', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_B_Digital_I']="%.3f" % fetchLastTSVarDB('SCC_B_Digital_I', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_B_Analog_I']="%.3f" % fetchLastTSVarDB('SCC_B_Analog_I', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_B_NTC']="%.1f" % fetchLastTSVarDB('SCC_B_NTC', 'ARDUINO_IO',dbCursor)

    outVars['ArduinoIO.SCC_C_Vdda']="%.2f" % fetchLastTSVarDB('SCC_C_Vdda', 'ARDUINO_IO',dbCursor)    
    outVars['ArduinoIO.SCC_C_Vddd']="%.2f" % fetchLastTSVarDB('SCC_C_Vddd', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_C_Digital_I']="%.3f" % fetchLastTSVarDB('SCC_C_Digital_I', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_C_Analog_I']="%.3f" % fetchLastTSVarDB('SCC_C_Analog_I', 'ARDUINO_IO',dbCursor)
    outVars['ArduinoIO.SCC_C_NTC']="%.1f" % fetchLastTSVarDB('SCC_C_NTC', 'ARDUINO_IO',dbCursor)

    #### Table Log
    maxLogMessages=100
    LogTime = [ unicode(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(v))) for v in fetchVarListDB('TS', 'Log', dbCursor, maxLogMessages) ]
    LogID = fetchVarListDB('ID', 'Log', dbCursor, maxLogMessages)
    LogSource = fetchVarListDB('Source_ID', 'Log', dbCursor, maxLogMessages)
    LogType = fetchVarListDB('Type_ID', 'Log', dbCursor, maxLogMessages)
    LogMessage = fetchVarListDB('Message', 'Log', dbCursor, maxLogMessages)
    outVars['Log.LogTable'] = formatForLogTable(LogID, LogSource, LogType, LogMessage, LogTime)

    #### Table Chip_Data
    outVars['Chip_Data.TS'] = unicode(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(fetchLastTSVarDB('TS', 'Chip_Data', dbCursor))))
    outVars['Chip_Data.Scan_Type'] = "%d" % fetchLastTSVarDB('Scan_Type', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_Ring_Oscillators_Avg_1'] = "%d" % fetchLastScanTypeDB('(Ring_Oscillators_1+Ring_Oscillators_2+Ring_Oscillators_3+Ring_Oscillators_4)*0.25', dbCursor, 2)
    outVars['Chip_Data.SCC_A_Ring_Oscillators_Avg_2'] = "%d" % fetchLastScanTypeDB('(Ring_Oscillators_5+Ring_Oscillators_6+Ring_Oscillators_7+Ring_Oscillators_8)*0.25', dbCursor, 2)
    outVars['Chip_Data.SCC_A_Iref'] = "%.2f" % ( fetchLastTSVarDB('IRef', 'Chip_Data', dbCursor) * 0.1 )
    outVars['Chip_Data.SCC_A_Occupancy_FE_1'] = "%.2f" % fetchLastTSVarDB('AnalogScan_Average_Occupancy_FE_1', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_Occupancy_FE_2'] = "%.2f" % fetchLastTSVarDB('AnalogScan_Average_Occupancy_FE_2', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_Occupancy_FE_3'] = "%.2f" % fetchLastTSVarDB('AnalogScan_Average_Occupancy_FE_3', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Mean_FE_1'] = "%.2f" % fetchLastTSVarDB('ThrScan_Mean_FE_1', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Mean_FE_2'] = "%.2f" % fetchLastTSVarDB('ThrScan_Mean_FE_2', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Mean_FE_3'] = "%.2f" % fetchLastTSVarDB('ThrScan_Mean_FE_3', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Sigma_FE_1'] = "%.2f" % fetchLastTSVarDB('ThrScan_Sigma_FE_1', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Sigma_FE_2'] = "%.2f" % fetchLastTSVarDB('ThrScan_Sigma_FE_2', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Sigma_FE_3'] = "%.2f" % fetchLastTSVarDB('ThrScan_Sigma_FE_3', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Noise_Mean_FE_1'] = "%.2f" % fetchLastTSVarDB('ThrScan_Noise_Mean_FE_1', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Noise_Mean_FE_2'] = "%.2f" % fetchLastTSVarDB('ThrScan_Noise_Mean_FE_2', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Noise_Mean_FE_3'] = "%.2f" % fetchLastTSVarDB('ThrScan_Noise_Mean_FE_3', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Noise_Sigma_FE_1'] = "%.2f" % fetchLastTSVarDB('ThrScan_Noise_Sigma_FE_1', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Noise_Sigma_FE_2'] = "%.2f" % fetchLastTSVarDB('ThrScan_Noise_Sigma_FE_2', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ThrScan_Noise_Sigma_FE_3'] = "%.2f" % fetchLastTSVarDB('ThrScan_Noise_Sigma_FE_3', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ToTScan_Mean_FE_1'] = "%.2f" % fetchLastTSVarDB('ToTScan_Mean_FE_1', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ToTScan_Mean_FE_2'] = "%.2f" % fetchLastTSVarDB('ToTScan_Mean_FE_2', 'Chip_Data', dbCursor)
    outVars['Chip_Data.SCC_A_ToTScan_Mean_FE_3'] = "%.2f" % fetchLastTSVarDB('ToTScan_Mean_FE_3', 'Chip_Data', dbCursor)

    outVars['Chip_Data.SCC_B_Iref']='0.0'
    outVars['Chip_Data.SCC_C_Iref']='0.0'
    
    return outVars

###### MAIN ###############
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Generate data for web page based on DB information.', usage='%(prog)s [options]', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--output', default='WPGVars.txt', type=str, help='Enable debug logging')
    
    parser.add_argument('--host', default='localhost',  help='Database Host') 
    parser.add_argument('--database', default='slipper', help='Name of Database')
    parser.add_argument('--user', default='slipper', help='Database username')
    parser.add_argument('--password', default='pxKr_LOG',  help='Database password')
    
    parser.add_argument('--LogFile', default='WPG.log',      help='Target Version Control TXT file')
    parser.add_argument('--LogTable', default='Log',         help='Target Error Table')
    
    parser.add_argument('--Debug', default=False,        action='store_true', help='Enable debug logging')
   
    args = parser.parse_args(sys.argv[1:])

    if(Debug):
        Log(Log_Service.DEBUG, "Starting WebPage_FetchData.py", 'WPG')
        
    #Settings
    Debug=args.Debug

    Log_Service.log_table=args.LogTable
    Log_Service.log_file_route=args.LogFile
    Log_Service.L_Key=Log_In_Key(args.host,args.user,args.password,args.database,args.LogTable)
    
    #Open connection to DB
    if Debug:
        Log(Log_Service.DEBUG, 'Connecting to Database using Host:'+args.host+' User:'+args.user, "WPG")
    try:
        db=MySQLdb.connect(args.host,args.user,args.password)
        cursor=db.cursor()
        cursor.execute('use '+args.database)
    except Exception, e:
      Log_DB(Log_Service.ERROR, 'Error connecting to DB: ' + str(e), 'WPG')
      sys.exit(1)
                    
    #Fetch data that we may need
    outputDict = fetchData(cursor)

    #Store output dictionary into a text file
    outFile = open(args.output, 'w')
    for k in outputDict:
        outFile.write('%s=%s\n' % (k,outputDict[k]))
    outFile.close()

    #Clean-up and exit
    db.close()
    sys.exit(0)

#!/usr/bin/env python
##
## Description:
##  Handles error and message logging to file and DB
##  Each message is characterize by a source and a type (unique error/info code)
##  The two main functions are:
##   - Log(msg_type,msg_string,msg_source)
##   - Log_DB(msg_type,msg_string,msg_source)
##  writing to file or DB respectively.
##
##  A type < 0 is identified as a debug message.
##  A type == 0 is identified as an informational message.
##  A type > 0 && < 100 is identified as a WARNING
##  A type >= 100 is identified as an ERROR
##  Please ensure to keep the mapping functions below up-to-date
##
## Authors:
##  Yuxuan Zhang
##  Simone Pagan Griso
##  Aleksandra Dimitrievska
## 
import sys
import argparse
import time
import MySQLdb
import os.path
from datetime import datetime

###### SETTINGS ###############

## Flag to turn-on debugging of logging service
Debug=False
Show_All_Possible_Error=False

## Info for log file and version history file
log_file_route=''

## Class to store information for DB logging
class Log_In_Key():
  Host='NA'
  ID='NA'
  PW='NA'
  DB='NA'
  Table='NA'
  def __init__(self, Host, ID, PW, DB, Table):
      self.Host = Host
      self.ID = ID
      self.PW = PW
      self.DB = DB
      self.Table = Table
L_Key=Log_In_Key('localhost','slipper','pxKr_LOG','slipper', 'Log')

##############################
## Utility to identify a Source_ID
def Source_Identify(msg_string):
  if(msg_string.lower()=='LOG'.lower()): return 0
  if(msg_string.lower()=='VERSION'.lower()): return 0
  if(msg_string.lower()=='Arduino_IO'.lower()): return 1
  if(msg_string.lower()=='WPG'.lower()): return 2
  if(msg_string.lower()=='SSL'.lower()): return 3
  if(msg_string.lower()=='D2R'.lower()): return 4  
  return -1

## Utility to identify a Source_ID
def Source_To_Str(msg_string):
  if (msg_string == 0): return 'LOG'
  if (msg_string == 1): return 'Arduino_IO'
  if (msg_string == 2): return 'WPG'
  if (msg_string == 3): return 'SSL'
  if (msg_string == 4): return 'D2R'  
  return 'Unknown'

## Utility to identify a Type_ID
def Type_To_Str(msg_type):
  if (msg_type < 0): return 'DEBUG'
  if (msg_type == 0): return 'INFO'
  if (msg_type < 100): return 'WARNING'
  if (msg_type >= 100): return 'ERROR'

## Discouraged, but allows default values for common messages
DEBUG = -1
INFO = 0
WARNING = 10
ERROR = 100

##############################
## Internal convenience variables
msg_source_ID = -1
msg_source_str =''
msg_type_ID = -1
msg_type_str = ''
##############################


def parse_msg_args(msg_type, msg_source):
  global msg_source_ID
  global msg_source_str
  global msg_type_ID
  global msg_type_str
  msg_source_ID = -1
  msg_source_str = ''
  if (type(msg_source) == int):
    msg_source_ID = msg_source
    msg_source_str = Source_To_Str(msg_source_ID)
  elif (type(msg_source) == str):
    msg_source_ID=Source_Identify(msg_source)
    msg_source_str = msg_source
  else:
    Log(ERROR, 'FIXME. Invalid message source submitted. Using -1', 'LOG')
    msg_source_ID=-1
    msg_source_str = Source_To_Str(msg_source_ID)

  msg_type_str = ''
  msg_type_ID = -1
  if (type(msg_type) == int):
    msg_type_ID = msg_type
    msg_type_str = Type_To_Str(msg_type)
  else:
    Log(ERROR, 'FIXME. Invalid message type submitted.', 'LOG')
    msg_type_ID = -1
    msg_type_str = Type_To_Str(msg_type_ID) 


## Add log entry to the DB (and to file)
def Log_DB(msg_type,msg_string,msg_source,stamp=0):
  msg_string=msg_string.replace('\n',' ')
  msg_string=msg_string.replace('\r',' ')

  parse_msg_args(msg_type, msg_source)

  if Debug:
    print(msg_source_str+' >> ' + msg_type_str + ' >> ' + msg_string)

  Log(msg_type, msg_string, msg_source) 
  if(L_Key.Table!=''):
    try:
      db=MySQLdb.connect(L_Key.Host, L_Key.ID, L_Key.PW)
      cursor = db.cursor()
      SQL_CMD="use "+L_Key.DB
      cursor.execute(SQL_CMD)
    except Exception, e:
      Log(ERROR, 'Cannot commit log message to DB. Error connecting to DB: ' + str(e), 'LOG')
      return 0
    if (stamp == 0):
      stamp=(int)(time.time())
    INSERT_CMD=( "INSERT INTO "+L_Key.Table
                 + " (TS, Source_ID, Type_ID, Message) VALUES("
                 + '%d' % stamp + ", "
                 + '%d' % msg_source_ID  + ", "
                 + '%d' % msg_type_ID    + ", "
                 + "'"  + msg_string + "' " + ");")
    #print(INSERT_CMD)
    try:
      cursor.execute(INSERT_CMD)
      db.commit()
      db.close()
    except Exception, e:
      Log(ERROR, 'Cannot commit log message to DB. Error writing to table: ' + str(e), 'LOG')
      return 0      
  else:
    Log(ERROR, 'Cannot commit log message to DB. Table undefined', 'LOG')


## Add log entry to file
def Log(msg_type, msg_string, msg_source):
  global log_file_route
  parse_msg_args(msg_type, msg_source)

  if Debug:
    print(msg_source_str+' >> ' + msg_type_str + ' >> ' + msg_string)
    print('Output file: ', log_file_route)
  if(log_file_route!=''): 
    try:        
      err_out = open(log_file_route, 'a')
      err_out.write(time.strftime("%Y-%m-%d %H:%M:%S, ", time.localtime()) +
                    msg_source_str +' (' + str(msg_source_ID) + ')' + 
                    ' >> ' + msg_type_str + ' (' + str(msg_type_ID) + ')' + 
                    ' >> ' + msg_string + '\n') 
      err_out.close()
    except Exception, e:
      print('LOG >> ERROR >> Could not write message to log file: ' + str(e))
      log_file_route=''
      #Do Nothing


## Utility to log in change of version
def version_control(version):
  #try:
  if Debug:
      Log(INFO,
          'Version_Control >> Connecting to Database using '+L_Key.Host+' '+L_Key.ID+' '+L_Key.PW+' '+L_Key.DB,
          "LOG")
  db=MySQLdb.connect(L_Key.Host, L_Key.ID, L_Key.PW)
  cursor = db.cursor()
  cursor.execute("use "+L_Key.DB)
  cursor.execute("SELECT MSG_Index From Log WHERE ID=1")
  line = cursor.fetchone()
  db.close()
  prev_version=line[0]
  if (version!=prev_version):
    if (prev_version.count('V')>0):
      Log_DB(0, 'From '+prev_version+' To '+version, "VERSION")
      SQL_CMD="UPDATE "+log_table+" SET MSG_Index='"+version+"' WHERE ID=1"
      #print SQL_CMD
      db=MySQLdb.connect(L_Key.Host, L_Key.ID, L_Key.PW)
      cursor = db.cursor()
      cursor.execute("use "+L_Key.DB)
      cursor.execute(SQL_CMD)
      db.commit()
      db.close()
    else:
      Log_DB(0, 'First Launch: '+version, "VERSION")
      SQL_CMD="UPDATE "+log_table+" SET MSG_Index='"+version+"' WHERE ID=1"
      #print SQL_CMD
      db=MySQLdb.connect(L_Key.Host, L_Key.ID, L_Key.PW)
      cursor = db.cursor()
      cursor.execute("use "+L_Key.DB)
      cursor.execute(SQL_CMD)
      db.commit()
      db.close()
    try:
      if (note_file_route!=''):
        note_file=open(note_file_route,'r')
        content=note_file.read()
        Log_ADD(0, version+" Upgrade Note:\n"+content.replace("\n"," "), "VERSION")
    except Exception, e:
      Log(ERROR, "Unable to update version history file: " + str(e), "VERSION")
  
 
if __name__ == '__main__':
    
  ## TODO: rewrite as utility to parse/add/quert log table
  parser = argparse.ArgumentParser(description='Interface with slipper logging system.', usage='%(prog)s [options]', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--message', required=True, default='', help='Message')
  parser.add_argument('--source', default=0, help='Specify source ID')
  parser.add_argument('--type', default=0, help='Specify type ID')
  parser.add_argument('--timestamp',      type=float,      default=0,      help='timestamp')  
  parser.add_argument('--Version', default='', help='Specify this option if performing an upgrade, and give the new version number')
  parser.add_argument('--db', default=False, action='store_true', help='Send message to DB, in addition to the log file')

  parser.add_argument('--host', default='localhost', help='Database LOG username')
  parser.add_argument('--user', default='Log_Upd', help='Database LOG username')
  parser.add_argument('--password', default='pxKr_LOG',   help='Database LOG password')
  parser.add_argument('--database', default='slipper', help='Name of Database')

  parser.add_argument('--LogTable', default='Log', help='Target Error Table')
  parser.add_argument('--LogFile', default='/local/slipper/slipper-data/perm/log/Others.log', help='UpGrade_note_file_Path')
  parser.add_argument('--Debug', default=False, action='store_true', help='Do not print to screen.')

  parser.add_argument('--Show_All_Possible_Error', default=False, action='store_true', help='Do not print to screen.')

  args = parser.parse_args(sys.argv[1:])

  Debug=args.Debug
  Show_All_Possible_Error=args.Show_All_Possible_Error

  log_file_route=args.LogFile
  log_table=args.LogTable
  L_Key=Log_In_Key(args.host,args.user,args.password,args.database, args.LogTable)

  if args.timestamp==0: args.timestamp=(int)(time.time())
  
  if (args.Version != ''):
    ## This is a version upgrade
    version_control(args.Version)
    sys.exit(0)

  if (args.db):
    Log_DB(int(args.type), args.message, int(args.source), args.timestamp)
  else:
    Log(int(args.type), args.message, int(args.source))
  
  sys.exit(0)

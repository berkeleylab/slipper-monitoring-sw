# First Launch Instructions

## Step 1: setup The Database

* Install Mariadb (if not already installed)
  * `sudo yum install mariadb`

* Configure SQL root Access
  * If you already have an SQL installed and don't remember the password you can reset it with the following:
    * Stop the database service: `$ sudo service mariadb stop`
    * Release the root access for SQL: `mysqld_safe --skip-grant-table &`
    * Now start a new shell window
    * run:
    ```
    mysql -u root
    UPDATE user SET Password=PASSWORD('newpassword') where USER='root';
    //create root user and update password
    DELETE FROM user WHERE user='';
    FLUSH PRIVILEGES;
    exit
    ```
    * Remember to replace 'newpassword' with your own passcode, do not put in the comments.
  * Enable mariadb to start at boot with `systemctl enable mariadb`
  * Start the service with `systemctl start mariadb`

* Create Databases and Tables
  * In order to make the mysql service run as expected, we need to setup desired database and datatables for it.
  * The names of the datatables are defined in the configuration file (config.sh) and you can change them if necessary. But the following commands will be using the default names.
  * Login to the database using root account: `mysql -u root -p`
  * Now create the needed database and tables as detailed [here](docs/DBStructure.md)

## Step 2: setup web server
* Setup a web server with a public_html folder
* Copy the content of the `WebPage/` folder into your public html area, e.g.
```
cp -r WebPage/* /home/slipper/public_html/
```
* Edit `scripts/config.sh` to set the correct web home and the correct source (template) and destination (web page) paths for the WPG program in the `WPG_file_stream` variable.

#### Step 3: setup Thar
* Clone Thar repository: git clone --recursive https://gitlab.cern.ch/adimitri/Thar.git
* Compile
* Create a running directory called `run` within the build folder (currently, the main Thar folder, e.g. Thar/run)
* Edit the config of this package (scripts/config.sh) to set the correct path to Thar

#### Step 4
* Adjust system settings
  * Add rules to udev to ensure consistent naming of USB devices across PC reboots. 
    * Determine the idVendor, idProduct and Serial number of the USB devices (Power supplies, Arduino) using e.g.:
      ```
      $ lsusb
      $ udevadm info -a -n /dev/usbtmc0 | grep serial| head -n 1
      ```
    * Add new rules to `/etc/udev/rules.d/99-usb.rules`
      ```
      SUBSYSTEM=="usb", ATTRS{idVendor}=="1ab1", ATTRS{idProduct}=="0e11", ATTRS{serial}=="DP8C202301871", SYMLINK+="usbps0"
      SUBSYSTEM=="usb", ATTRS{idVendor}=="1ab1", ATTRS{idProduct}=="0e11", ATTRS{serial}=="DP8C202301819", SYMLINK+="usbps1"
      SUBSYSTEM=="tty", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0043", ATTRS{serial}=="757353030323511092F1", SYMLINK+="ttyArd0"            
      ```    
      also add to `/etc/udev/rules.d/99-xusb.rules`
      ```
      ATTRS{idVendor}=="1ab1", ATTRS{idProduct}=="0e11", MODE="666"
      ```   
      and reload rules with:
      ```
      sudo udevadm control --reload-rules && udevadm trigger
      ```
* Adjust configurations
  * Create output folders:
    ```
    mkdir /local/slipper/slipper-data    
    mkdir /local/slipper/slipper-data/perm
    mkdir /local/slipper/slipper-data/perm/custom-scans
    mkdir /local/slipper/slipper-data/perm/scan
    mkdir /local/slipper/slipper-data/perm/tuning
    mkdir /local/slipper/slipper-data/perm/log
    mkdir /local/slipper/slipper-data/perm/db
    mkdir /local/slipper/slipper-data/temp/
    mkdir /local/slipper/slipper-data/temp/scan-log/
    mkdir /local/slipper/slipper-data/temp/WPG/
    ```
  * Edit `scripts/config.sh` to adjust other paths accordingly to the installation
  * Ensure that `scripts/config.sh` has the correct links to the usb devices, according to the udev rules above
  * Optionally, you can edit the python programs to adjust defaults to your configuration

#### Step 5
Edit the file `slipper.crontab` to adjust the paths according to the installation.

Install its content into the user crontab with:
```
crontab slipper.crontab
```

To disable or edit part of it just edit the crontab directly with `crontab -e`, or `crontab -r` to remove them all.

#### Step 6
A Raid1 will be used with a mirrored disk.
Additionally, the folder `/local/slipper/slipper-data/permanent/` needs to be backed up remotely.
The database is dumped into the backup folder daily via a cronjob.

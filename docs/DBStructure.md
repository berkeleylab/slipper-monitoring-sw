--------------------------------------------------------------------------------
This file details the most recent version of the database tables
--------------------------------------------------------------------------------

## Create database
```
mysql/mariadb>
CREATE DATABASE slipper;
USE slipper;
```

## Create tables

#### ARDUINO_IO
```
mysql> 
CREATE TABLE ARDUINO_IO (
TS INT NOT NULL COMMENT 'Timestamp, in Unix Epoch',
Env_Temp FLOAT default 0 COMMENT 'Environment temperature [C]',
Env_Humidity FLOAT default 0 COMMENT 'Enviroment humidity [%]',
SCC_A_Vddd FLOAT default 0 COMMENT 'Single-Chip-Card A, Digital voltage [V]',
SCC_A_Vdda FLOAT default 0 COMMENT 'Single-Chip-Card A, Analog voltage [V]',
SCC_A_VRefd FLOAT default 0 COMMENT 'Single-Chip-Card A, VRef digital voltage [V]',
SCC_A_VRefa FLOAT default 0 COMMENT 'Single-Chip-Card A, VRef analog voltage [V]',
SCC_A_NTC FLOAT default 0 COMMENT 'Single-Chip-Card A, NTC temperature reading [C]',
SCC_A_Digital_I FLOAT default 0 COMMENT 'Single-Chip-Card A, digital current reading [uA]',
SCC_A_Analog_I FLOAT default 0 COMMENT 'Single-Chip-Card A, analog current reading [uA]',
SCC_B_Vddd FLOAT default 0 COMMENT 'Single-Chip-Card B, Digital voltage [V]',
SCC_B_Vdda FLOAT default 0 COMMENT 'Single-Chip-Card B, Analog voltage [V]',
SCC_B_VRefd FLOAT default 0 COMMENT 'Single-Chip-Card B, VRef digital voltage [V]',
SCC_B_VRefa FLOAT default 0 COMMENT 'Single-Chip-Card B, VRef analog voltage [V]',
SCC_B_NTC FLOAT default 0 COMMENT 'Single-Chip-Card B, NTC temperature reading [C]',
SCC_B_Current FLOAT default 0 COMMENT 'Single-Chip-Card B, current reading [uA]',
SCC_B_Digital_I FLOAT default 0 COMMENT 'Single-Chip-Card B, digital current reading [uA]',
SCC_B_Analog_I FLOAT default 0 COMMENT 'Single-Chip-Card B, analog current reading [uA]',
SCC_C_Vddd FLOAT default 0 COMMENT 'Single-Chip-Card C, Digital voltage [V]',
SCC_C_Vdda FLOAT default 0 COMMENT 'Single-Chip-Card C, Analog voltage [V]',
SCC_C_VRefd FLOAT default 0 COMMENT 'Single-Chip-Card C, VRef digital voltage [V]',
SCC_C_VRefa FLOAT default 0 COMMENT 'Single-Chip-Card C, VRef analog voltage [V]',
SCC_C_NTC FLOAT default 0 COMMENT 'Single-Chip-Card C, NTC temperature reading [C]',
SCC_C_Digital_I FLOAT default 0 COMMENT 'Single-Chip-Card C, digital current reading [uA]',
SCC_C_Analog_I FLOAT default 0 COMMENT 'Single-Chip-Card C, analog current reading [uA]',
PRIMARY KEY (TS)
);
```


#### Log
```
mariadb> 
CREATE TABLE Log (
ID INT NOT NULL AUTO_INCREMENT,
TS INT NOT NULL COMMENT 'Timestamp, in Unux Epoch',
Source_ID INT NOT NULL COMMENT 'source program unique identifier',
Type_ID INT NOT NULL COMMENT 'type of message from source',
Message VARCHAR(200),
PRIMARY KEY (ID)
);
```

Optionally, insert a test message stating the version of the upgrade/installation:
```
INSERT INTO Log (TS,Source_ID,Type_ID,Message) VALUES(1534360031,0,0,'Version 6.0-pre installed');
```

Finally, some handly old commands that are reported here since they could be useful for updates:
```
ALTER TABLE Log 
MODIFY Date_Time timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL;
UPDATE Log SET MSG_Type='UPDATE' WHERE MSG_Type='Version_Update';
UPDATE Log SET MSG_Type='CURRENT' WHERE MSG_Type='Current_Version';
UPDATE Log SET MSG_Index='From V1.00 To V1.01' WHERE MSG_Index='V1.01' and MSG_Type='UPDATE';
ALTER TABLE Log 
MODIFY Date_Time timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP;
```

#### Source_Position
```
mariadb> 
CREATE TABLE Source_Position (
TS INT NOT NULL COMMENT 'Timestamp, in Unux Epoch',
Chip_ID INT NOT NULL COMMENT 'Identifier of SCC. 1=SCC-A, 2=SCC-B, 3=SCC-C',
Position INT NOT NULL COMMENT 'Position of radioactive source. 0=absent, 1=left, 2=right',
PRIMARY KEY (TS)
);
```

#### Chip_Data
```
mariadb> 
CREATE TABLE Chip_Data (
ID INT NOT NULL AUTO_INCREMENT,
TS INT NOT NULL COMMENT 'Timestamp, in Unux Epoch',
Chip_ID INT NOT NULL COMMENT 'Identifier of SCC. 1=SCC-A, 2=SCC-B, 3=SCC-C',
Source_Position INT NOT NULL COMMENT 'Position of radioactive source. 0=absent, 1=left, 2=right',
Scan_Type INT DEFAULT 0 COMMENT 'Type of scan',
Data_Path VARCHAR(200) COMMENT 'Path to saved scan data, if any',
Temperature_Sensor_1 FLOAT COMMENT 'Temperature (1) of chip, as read internally',
Temperature_Sensor_2 FLOAT COMMENT 'Temperature (2) of chip, as read internally',
Temperature_Sensor_3 FLOAT COMMENT 'Temperature (3) of chip, as read internally',
Temperature_Sensor_4 FLOAT COMMENT 'Temperature (4) of chip, as read internally',
Radiation_Sensor_1 FLOAT COMMENT 'Radiation dose (1) of chip, as read internally',
Radiation_Sensor_2 FLOAT COMMENT 'Radiation dose (2) of chip, as read internally',
Radiation_Sensor_3 FLOAT COMMENT 'Radiation dose (3) of chip, as read internally',
Radiation_Sensor_4 FLOAT COMMENT 'Radiation dose (4) of chip, as read internally',
IRef FLOAT COMMENT 'IRef [uA]',
Ring_Oscillators_1 FLOAT COMMENT 'Ring Oscillator (1) readings',
Ring_Oscillators_2 FLOAT COMMENT 'Ring Oscillator (2) readings',
Ring_Oscillators_3 FLOAT COMMENT 'Ring Oscillator (3) readings',
Ring_Oscillators_4 FLOAT COMMENT 'Ring Oscillator (4) readings',
Ring_Oscillators_5 FLOAT COMMENT 'Ring Oscillator (5) readings',
Ring_Oscillators_6 FLOAT COMMENT 'Ring Oscillator (6) readings',
Ring_Oscillators_7 FLOAT COMMENT 'Ring Oscillator (7) readings',
Ring_Oscillators_8 FLOAT COMMENT 'Ring Oscillator (8) readings',
DigiScan_Average_Occupancy_FE_1 FLOAT COMMENT 'Average occupancy of digital scan, FE 1',
AnalogScan_Average_Occupancy_FE_1 FLOAT COMMENT 'Average occupancy of analog scan, FE 1',
ThrScan_Mean_FE_1 FLOAT COMMENT 'Threshold scan, S-curve mean, FE 1',
ThrScan_Sigma_FE_1 FLOAT COMMENT 'Threshold scan, S-curve sigma, FE 1',
ThrScan_Noise_Mean_FE_1 FLOAT COMMENT 'Threshold scan, noise mean, FE 1',
ThrScan_Noise_Sigma_FE_1 FLOAT COMMENT 'Threshold scan, noise sigma, FE 1',
ToTScan_Mean_FE_1 FLOAT COMMENT 'Mean of ToT distribution for FE 1',
DigiScan_Average_Occupancy_FE_2 FLOAT COMMENT 'Average occupancy of digital scan, FE 2',
AnalogScan_Average_Occupancy_FE_2 FLOAT COMMENT 'Average occupancy of analog scan, FE 2',
ThrScan_Mean_FE_2 FLOAT COMMENT 'Threshold scan, S-curve mean, FE 2',
ThrScan_Sigma_FE_2 FLOAT COMMENT 'Threshold scan, S-curve sigma, FE 2',
ThrScan_Noise_Mean_FE_2 FLOAT COMMENT 'Threshold scan, noise mean, FE 2',
ThrScan_Noise_Sigma_FE_2 FLOAT COMMENT 'Threshold scan, noise sigma, FE 2',
ToTScan_Mean_FE_2 FLOAT COMMENT 'Mean of ToT distribution for FE 2',
DigiScan_Average_Occupancy_FE_3 FLOAT COMMENT 'Average occupancy of digital scan, FE 3',
AnalogScan_Average_Occupancy_FE_3 FLOAT COMMENT 'Average occupancy of analog scan, FE 3',
ThrScan_Mean_FE_3 FLOAT COMMENT 'Threshold scan, S-curve mean, FE 3',
ThrScan_Sigma_FE_3 FLOAT COMMENT 'Threshold scan, S-curve sigma, FE 3',
ThrScan_Noise_Mean_FE_3 FLOAT COMMENT 'Threshold scan, noise mean, FE 3',
ThrScan_Noise_Sigma_FE_3 FLOAT COMMENT 'Threshold scan, noise sigma, FE 3',
ToTScan_Mean_FE_3 FLOAT COMMENT 'Mean of ToT distribution for FE 3',
VCal_High_Left_Offset FLOAT COMMENT 'Vcal offset results for High/Left case [V]',
VCal_High_Left_Slope FLOAT COMMENT 'Vcal slope results for High/Left case [1/V]',
VCal_High_Right_Offset FLOAT COMMENT 'Vcal offset results for High/Right case [V]',
VCal_High_Right_Slope FLOAT COMMENT 'Vcal slope results for High/Right case [1/V]',
VCal_Medium_Left_Offset FLOAT COMMENT 'Vcal offset results for Medium/Left case [V]',
VCal_Medium_Left_Slope FLOAT COMMENT 'Vcal slope results for Medium/Left case [1/V]',
VCal_Medium_Right_Offset FLOAT COMMENT 'Vcal offset results for Medium/Right case [V]',
VCal_Medium_Right_Slope FLOAT COMMENT 'Vcal slope results for Medium/Right case [1/V]',
PRIMARY KEY (ID)
);
```

## Create users and grant permissions

A single user is currently used from the code to write into the database.
To set it up correctly, you can create the user and grant the needed permissions as follows:

```
$ mysql -u root -p
mariadb>
GRANT SELECT,INSERT,UPDATE,DELETE,DROP,CREATE,ALTER,LOCK TABLES ON slipper.* TO 'slipper'@'localhost' IDENTIFIED BY 'pxKr_LOG' WITH GRANT OPTION;
FLUSH Privileges;
```
